package mx.rmr.make.encuestas

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONArrayRequestListener
import com.androidnetworking.interfaces.JSONObjectRequestListener
import kotlinx.android.synthetic.main.activity_encuestas.*
import mx.rmr.make.*
import mx.rmr.make.eventos.ListenerConsultaEvento
import mx.rmr.make.util.Comun
import org.json.JSONArray
import org.json.JSONObject

class EncuestasActiv : AppCompatActivity(), ListenerConsultaEvento
{
    private var arrJsonEncuestas = JSONArray()
    private var aliasActual = ""

    private var idIsla: Int = 0
    // Diccionario de la isla
    private lateinit var dIsla: JSONObject

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_encuestas)

        //cargarIslaSeleccionada()
        descargarIsla()
    }

    private fun descargarIsla() {
        habilitarEntrada(false)
        //pbEsperaEncuestas.visibility = View.VISIBLE

        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")
        idIsla = prefs.getInt(LLAVE_ID_ISLA_SELECCIONADA, 0)

        val direccion = URL_LEER_ISLA.replace(":id", "$idIsla")

        AndroidNetworking.get(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    //pbEsperaEncuestas.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    dIsla = response!!
                }

                override fun onError(anError: ANError?) {
                    //pbEsperaEncuestas.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    when (anError?.errorCode) {
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(this@EncuestasActiv, MENSAJE_NO_INTERNET)
                        }
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@EncuestasActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        else -> {
                            Comun.mostrarMensaje(this@EncuestasActiv, MENSAJE_ERROR_GENERAL)
                        }
                    }
                }
            })
    }

    override fun onResume() {
        super.onResume()
        descargarEncuestas()
    }

    private fun descargarEncuestas() {
        //pbEsperaEncuestas.visibility = View.VISIBLE
        habilitarEntrada(false)

        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")
        val idIsla = prefs.getInt(LLAVE_ID_ISLA_SELECCIONADA, 0)
        aliasActual = prefs.getString(LLAVE_ALIAS, "")!!

        val direccion = URL_LISTA_ENCUESTAS.replace(":id", "$idIsla")

        AndroidNetworking.get(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .build()
            .getAsJSONArray(object: JSONArrayRequestListener {
                override fun onResponse(response: JSONArray?) {
                    //pbEsperaEncuestas.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    extraerEncuestas(response)
                }

                override fun onError(anError: ANError?) {
                    //pbEsperaEncuestas.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    when (anError?.errorCode) {
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(this@EncuestasActiv, MENSAJE_NO_INTERNET)
                        }
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(this@EncuestasActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        else -> {
                            Comun.mostrarMensaje(this@EncuestasActiv, MENSAJE_ERROR_GENERAL)
                        }
                    }
                }
            })
    }

    private fun extraerEncuestas(response: JSONArray?) {
        arrJsonEncuestas = response!!

        var arrEncuestas = mutableListOf<Encuesta>()  // Para el adaptador
        for (i in 0 until response!!.length()) {
            val dEncuesta = response.getJSONObject(i)
            val titulo = dEncuesta.getString("title")
            val contestada = estaContestada(dEncuesta)
            val encuesta = Encuesta(titulo, contestada)
            arrEncuestas.add(encuesta)
        }

        if (arrEncuestas.isEmpty()) {
            tvEncuestasDisponibles.text = "No hay encuestas disponibles."
        } else {
            tvEncuestasDisponibles.text = "Encuestas disponibles:"
        }

        // Recycler View
        var layout = LinearLayoutManager(this)
        layout.orientation = LinearLayoutManager.VERTICAL
        rvEncuestas.layoutManager = layout

        val adaptadorEncuestas = AdaptadorEncuestas(this, arrEncuestas.toTypedArray())
        rvEncuestas.adapter = adaptadorEncuestas

        val divisor = DividerItemDecoration(this, layout.orientation)
        rvEncuestas.addItemDecoration(divisor)
    }

    private fun estaContestada(dEncuesta: JSONObject?): Boolean {
        val arrVotos = dEncuesta!!.getJSONArray("votes")
        for (i in 0 until arrVotos.length()) {
            val dVoto = arrVotos.getJSONObject(i)
            val usuario = dVoto.getString("user")
            if (usuario == aliasActual) {
                return true
            }
        }
        return false
    }

    override fun btnClicked(indice: Int) {
        val dEncuesta = arrJsonEncuestas.getJSONObject(indice)
        val contestada = estaContestada(dEncuesta)
        if (contestada) {
            mostrarResultados(dEncuesta)
        } else {
            contestarEncuesta(dEncuesta)
        }
    }

    private fun contestarEncuesta(dEncuesta: JSONObject?) {
        //val descripcion = URLDecoder.decode(dEncuesta?.getString("description"), "UTF-8")
        val descripcion = dEncuesta?.getString("description")
        val idTopic = dEncuesta?.getInt("id")
        val arrOpcionesJson = dEncuesta?.getJSONArray("choices")!!
        val arrOpciones = arrayListOf<String>()
        for (i in 0 until arrOpcionesJson.length()) {
            val dOpcion = arrOpcionesJson.getJSONObject(i)
            arrOpciones.add(dOpcion.getString("title"))
        }
        // Menu con arrOpciones
        var indiceSeleccionado = 0
        val builder = AlertDialog.Builder(this)
            .setTitle("$descripcion")
            //.setIcon(R.drawable.pregunta)
            .setSingleChoiceItems(
                arrOpciones.toTypedArray(), 0
            ) { _, which ->
                indiceSeleccionado = which
            }
            .setPositiveButton("Aceptar")
            { _, _ ->
                // Enviar respuesta
                val id = arrOpcionesJson.getJSONObject(indiceSeleccionado).getInt("id")
                subirRespuesta(idTopic, id, arrOpciones[indiceSeleccionado])
            }
            .setNegativeButton("Cancelar", null)

        builder.create()
        builder.show()
    }

    private fun subirRespuesta(idTopic: Int?, idRespuesta: Int, respuesta: String) {
        //pbEsperaEncuestas.visibility = View.VISIBLE
        habilitarEntrada(false)

        // Token
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val direccion = URL_ENVIA_RESPUESTA

        var jsonBody = JSONObject(
            """
            {
            "topic": "$idTopic",
            "choice": $idRespuesta
            }
            """.trimIndent()
        )

        AndroidNetworking.post(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .addJSONObjectBody(jsonBody)
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    //pbEsperaEncuestas.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    val mensaje = response?.getString("message") ?: "Respuesta grabada"
                    Comun.mostrarMensaje(this@EncuestasActiv, mensaje)
                    descargarEncuestas()
                }

                override fun onError(anError: ANError?) {
                    //pbEsperaEncuestas.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    when (anError?.errorCode) {
                        ERROR_VOTO_REPETIDO -> {
                            Comun.mostrarMensaje(this@EncuestasActiv, "Error, voto emitido anteriormente")
                        }
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@EncuestasActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@EncuestasActiv, MENSAJE_NO_INTERNET, true)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@EncuestasActiv, MENSAJE_ERROR_GENERAL, true)
                        }
                    }
                }
            })
    }

    private fun mostrarResultados(dEncuesta: JSONObject?) {
        //println("dEncuesta: $dEncuesta")
        val modeloGob = dIsla.getString("governance_model")
        var usuarioCentral = ""
        if (modeloGob=="centralized") {
            usuarioCentral = dIsla.getString("centralized_user")
        }
        var pesoUsuarios = JSONObject()
        if (modeloGob=="weighted") {
            pesoUsuarios = dIsla.getJSONObject("user_weights")
        }
        val descripcion = dEncuesta?.getString("description")
        val dOpciones = dEncuesta?.getJSONArray("choices")
        // Crear diccionario [id:titulo]
        val dIdTitulo = mutableMapOf<Int, String>()
        var dVotos = mutableMapOf<Int, Int>()
        for (i in 0 until dOpciones!!.length()) {
            val dOpcion = dOpciones.getJSONObject(i)
            val idTitulo = dOpcion.getInt("id")
            dVotos[idTitulo] = 0
            dIdTitulo[idTitulo] = dOpcion.getString("title")
        }

        val arrVotos = dEncuesta.getJSONArray("votes")
        for (i in 0 until arrVotos.length()) {
            val dVoto = arrVotos.getJSONObject(i)
            val respuesta = dVoto.getJSONObject("vote_choice")
            val idVoto = respuesta.getInt("id")
            val aliasVoto = dVoto.getString("user")

            when (modeloGob) {
                "democratic" -> {
                  dVotos[idVoto] = dVotos[idVoto]!! + 1
                }
                "weighted" -> {
                    dVotos[idVoto] = dVotos[idVoto]!! + pesoUsuarios.getInt(aliasVoto)
                }
                "centralized" -> {
                    if (aliasVoto==usuarioCentral) {
                        dVotos[idVoto] = 1
                    }
                }
            }

            var mensaje = "Ya contestaste la encuesta, estas son las votaciones hasta el momento.\n\n"
            //mensaje += URLDecoder.decode(descripcion, "UTF-8")
            mensaje += descripcion
            mensaje += "\n\n"

            for (id in dVotos.keys.sorted()) {
                val totalVotos = dVotos[id]
                mensaje += "${dIdTitulo[id]} - $totalVotos votos\n"
            }

            val dialogo = AlertDialog.Builder(this)
                .setTitle("Resultados")
                .setMessage(mensaje)
                .setPositiveButton("Aceptar", null)
                .setCancelable(false)
                .create()
            dialogo.show()
        }
    }

    fun mostrarCreaEncuesta(v: View) {
        val intCreaEncuesta = Intent(this, CreaEncuestaActiv::class.java)
        startActivity(intCreaEncuesta)
    }

    private var entradaHabilitada = true

    private fun habilitarEntrada(habilitada: Boolean) {
        pbEsperaEncuestas.visibility = if (habilitada) View.INVISIBLE else View.VISIBLE
        entradaHabilitada = habilitada
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        return if (entradaHabilitada) super.dispatchTouchEvent(ev) else true
    }
}
