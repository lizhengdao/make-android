package mx.rmr.make.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import kotlinx.android.synthetic.main.activity_nueva_cuenta.*
import kotlinx.android.synthetic.main.activity_nueva_cuenta.etAlias
import mx.rmr.make.util.Comun
import mx.rmr.make.R
import mx.rmr.make.URL_SERVICIO_SIGN_UP
import org.json.JSONObject

class NuevaCuentaActiv : AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nueva_cuenta)

        supportActionBar?.hide()
    }

    fun mostrarAvisoPrivacidad(v: View) {
        val intPrivacidad = Intent(this, AvisoPrivacidadActiv::class.java)
        startActivity(intPrivacidad)
    }

    fun confirmarRegistro(v: View) {
        Comun.esconderTeclado(this)
        if (sonCamposValidos()) {
            if (checkAcepta.isChecked) {
                enviarRegistro()
            } else {
                Comun.mostrarMensaje(this, "Debe aceptar la política de privacidad antes de registrarse")
            }
        } else {
            Comun.mostrarMensaje(this, "Llene todos los campos")
        }
    }

    private fun enviarRegistro() {
        habilitarEntrada(false)

        val jsonBody = JSONObject("""{
            'username': '${etAlias.text.toString()}',
            'email': '${etCorreo.text.toString()}',
            'password': '${etContrasena1.text.toString()}',
            'password_confirmation': '${etContrasena2.text.toString()}'
        }
        """)

        AndroidNetworking.post(URL_SERVICIO_SIGN_UP)
            .addHeaders("Content-type", "application/json")
            .addJSONObjectBody(jsonBody)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    habilitarEntrada(true)
                    Comun.mostrarMensaje(this@NuevaCuentaActiv, "Registro exitoso.\nUn mensaje de correo le será enviado para confirmar su registro.", true)
                }

                override fun onError(anError: ANError?) {
                    habilitarEntrada(true)
                    if (anError!=null){
                        if (anError.errorCode != 0) {
                            val dError = JSONObject(anError.errorBody)
                            val mensaje = dError?.get("message") as String?
                            if (mensaje != null) {
                                Comun.mostrarMensaje(this@NuevaCuentaActiv,mensaje)
                            } else {
                                Comun.mostrarMensaje(this@NuevaCuentaActiv,"Error en los datos")
                            }
                        } else {    // Error en la red
                            Comun.mostrarMensaje(this@NuevaCuentaActiv, "Revisa tu conexión a la red")
                        }
                    }
                }
            })
    }

    private fun sonCamposValidos(): Boolean {
        val correo = etCorreo.text.toString()
        val alias = etAlias.text.toString()
        val contrasena1 = etContrasena1.text.toString()
        val contrasena2 = etContrasena2.text.toString()

        if (etAlias.text?.isEmpty()!!) {
            etAlias.setError("No puede estar vacío")
        }
        if (etCorreo.text?.isEmpty()!!) {
            etCorreo.setError("No puede estar vacío")
        }
        if (etContrasena1.text?.isEmpty()!!) {
            etContrasena1.setError("No puede estar vacío")
        }
        if (etContrasena2.text?.isEmpty()!!) {
            etContrasena2.setError("No puede estar vacío")
        }

        return correo.isNotEmpty() && alias.isNotEmpty()
                && contrasena1.isNotEmpty() && contrasena2.isNotEmpty()
    }

    private var entradaHabilitada = true

    private fun habilitarEntrada(habilitada: Boolean) {
        pbEspera.visibility = if (habilitada) View.INVISIBLE else View.VISIBLE
        entradaHabilitada = habilitada
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        return if (entradaHabilitada) super.dispatchTouchEvent(ev) else true
    }
}
