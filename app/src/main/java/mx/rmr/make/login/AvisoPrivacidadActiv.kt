package mx.rmr.make.login

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.DownloadListener
import android.webkit.WebView
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.activity_aviso_privacidad.*
import mx.rmr.make.R
import java.net.URL

class AvisoPrivacidadActiv : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aviso_privacidad)

        pbEspera.visibility = View.VISIBLE
    }

    override fun onResume() {
        super.onResume()
        webPrivacidad.loadUrl("file:///android_asset/AvisoPrivacidad.html")
        webPrivacidad.webViewClient = (object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                pbEspera.visibility = View.INVISIBLE
            }
        })
    }
}
