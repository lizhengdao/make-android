package mx.rmr.make.login

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import kotlinx.android.synthetic.main.activity_main.*
import mx.rmr.make.*
import mx.rmr.make.botonpanico.BotonPanicoActiv
import mx.rmr.make.menu.MenuMakeActiv
import mx.rmr.make.util.Comun
import org.json.JSONException
import org.json.JSONObject


/*
Pantalla de Login.
 */
open class LoginActiv : AppCompatActivity()
{
    private val LOGIN_BOTON_PANICO = "BotonPanico"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recuperarCredenciales()
        AndroidNetworking.initialize(baseContext)
    }

    private fun recuperarCredenciales() {
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE) ?: return
        val prefAlias = prefs.getString(LLAVE_ALIAS, "")
        val prefContrasena = prefs.getString(LLAVE_CONTRASENA, "")
        etAlias.setText(prefAlias)
        etContrasena.setText(prefContrasena)
    }

    fun hacerLogin(v: View) {
        Comun.esconderTeclado(this)
        if (sonCamposValidos()) {
            enviarSolicitudLogin(etAlias.text.toString(), etContrasena.text.toString())
        } else {
            Comun.mostrarMensaje(this,"Capture alias y contraseña")
        }
    }

    private fun enviarSolicitudLogin(alias: String, password: String, tipoLogin: String = "Normal") {
        //pbEspera.visibility = View.VISIBLE
        habilitarEntrada(false)

        val jsonBody = JSONObject("{ 'user': { 'username':'$alias', 'password':'$password'}}")
        AndroidNetworking.post(URL_SERVICIO_LOGIN)
            .addHeaders("Content-type", "application/json; charset=utf-8")
            .addJSONObjectBody(jsonBody)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    //pbEspera.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    guardarCredenciales(response)
                    if (tipoLogin==LOGIN_BOTON_PANICO) {
                        val intBotonPanico = Intent(this@LoginActiv, BotonPanicoActiv::class.java)
                        startActivity(intBotonPanico)
                    } else {
                        mostrarPantallaMenu()
                    }
                }

                override fun onError(anError: ANError?) {
                    //pbEspera.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    if (anError!=null){
                        if (anError.errorCode != 0) {
                            var dError: JSONObject? = null
                            try {
                                dError = JSONObject(anError.errorBody)
                            } catch (e: JSONException) {
                                Comun.mostrarMensaje(this@LoginActiv, MENSAJE_ERROR_GENERAL)
                                return
                            }
                            val mensaje = dError?.get("error") as String?
                            if (mensaje != null) {
                                Comun.mostrarMensaje(this@LoginActiv,mensaje)
                            } else {
                                Comun.mostrarMensaje(this@LoginActiv, MENSAJE_ERROR_GENERAL)
                            }
                        } else {    // Error en la red
                            Comun.mostrarMensaje(
                                    this@LoginActiv, MENSAJE_NO_INTERNET, false)
                        }
                    } else {
                        Comun.mostrarMensaje(this@LoginActiv, MENSAJE_ERROR_GENERAL)
                    }
                }
            })
    }

    private fun guardarCredenciales(response: JSONObject?) {
        val alias = etAlias.text.toString()
        val contrasena = etContrasena.text.toString()
        val token = response?.get("token") as String

        val preferencias = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE) ?: return
        val aliasPrevio = preferencias.getString(LLAVE_ALIAS, "")
        preferencias.edit().apply {
            putString(LLAVE_ALIAS, alias)
            putString(LLAVE_CONTRASENA, contrasena)
            putString(LLAVE_TOKEN, token)
            if (aliasPrevio != alias) {
                remove(LLAVE_ID_ISLA_SELECCIONADA)
                remove(LLAVE_JSON_ISLA)
            }
            commit()
        }
    }

    private fun mostrarPantallaMenu() {
        val intMenu = Intent(this, MenuMakeActiv::class.java)
        startActivity(intMenu)
        finish()
    }

    fun mostrarBotonPanico(v: View) {
        if (sonCamposValidos()) {
            enviarSolicitudLogin(etAlias.text.toString(), etContrasena.text.toString(), LOGIN_BOTON_PANICO)
        } else {
            Comun.mostrarMensaje(this,"Captura alias y contraseña")
        }
    }

    fun recuperarContrasena(v: View) {
        val direccion = Uri.parse(URL_SERVICIO_RESTAURAR_PASSWORD)
        val intBrowser = Intent(Intent.ACTION_VIEW, direccion)
        startActivity(intBrowser)
    }

    private fun sonCamposValidos(): Boolean {
        val alias = etAlias.text.toString()
        val contrasena = etContrasena.text.toString()
        if (alias.isEmpty() || contrasena.isEmpty()) {
            return false
        }
        return true
    }

    fun crearNuevaCuenta(v: View) {
        val intNuevaCuenta = Intent(this, NuevaCuentaActiv::class.java)
        startActivity(intNuevaCuenta)
    }

    private var entradaHabilitada = true

    private fun habilitarEntrada(habilitada: Boolean) {
        pbEspera.visibility = if (habilitada) View.INVISIBLE else View.VISIBLE
        entradaHabilitada = habilitada
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        return if (entradaHabilitada) super.dispatchTouchEvent(ev) else true
    }
}
