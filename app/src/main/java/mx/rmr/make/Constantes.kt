package mx.rmr.make

const val URL_SERVIDOR = "https://make-sec.org/"

// Signup
const val URL_SERVICIO_SIGN_UP = URL_SERVIDOR + "signup"    // post
// Login
const val URL_SERVICIO_LOGIN = URL_SERVIDOR + "login"       // post
const val OK_LOGIN = 201
const val ERROR_INFO_INVALIDA = 401
const val ERROR_PASSWORD_INVALIDO = 422
const val ERROR_NO_INTERNET = 0

// RESTAURAR PASSWORD
const val URL_SERVICIO_RESTAURAR_PASSWORD = "https://app.make-sec.org/password/reset"

// PREFERENCIAS
const val PREFS_CREDENCIALES = "Credenciales"
const val LLAVE_ALIAS = "Alias"
const val LLAVE_CONTRASENA = "Contrasena"
const val LLAVE_TOKEN = "Token"
const val LLAVE_ID_ISLA_SELECCIONADA = "Id_isla"
const val LLAVE_JSON_ISLA = "Json_isla"

const val PREFS_EVENTOS = "Eventos"
const val LLAVE_EVENTOS = "TiposEvento"

// ISLA no seleccionada
const val ISLA_NO_SELECCIONADA = -1

// EVENTOS del grupo
const val URL_GRUPO_INCIDENTES = URL_SERVIDOR + "user/groups/:id/incidents"   //  :id #isla, GET
const val URL_TIPOS_INCIDENTES = URL_SERVIDOR + "/incident_categories"
const val ERROR_NO_HAY_EVENTOS = 404

// ATTACHMENTS
const val URL_CREA_ATTACHMENT = URL_SERVIDOR + "user/attachments/"
const val ERROR_EXT_INVALIDA = 422
const val ERROR_TAM_INCORRECTO = 400

// EVENTOS
const val URL_CREA_EVENTO = URL_SERVIDOR + "/user/incidents/"
const val ERROR_CREA_EVENTO = 404
const val ERROR_REGISTRO_EVENTO_DUPLICADO = 400
const val URL_CREA_EVENTO_COMPLEMENTO = URL_CREA_EVENTO + ":id/complements/"
const val ERROR_REGISTRO_BORRAR_NO_EXISTE = 404

const val URL_INCIDENTE_BORRAR = URL_SERVIDOR + "user/incidents/"    // pegar ***id/

// AUXILIARES
const val NO_EXISTE = "NoExiste"
const val MENSAJE_CREDENCIALES_INVALIDAS = "Por cuestión de seguridad, cierra tu sesión y haz login nuevamente"
const val MENSAJE_NO_INTERNET = "Revisa tu conexión a la red"
const val MENSAJE_ERROR_GENERAL = "Error, intenta más tarde"
const val MENSAJE_NO_ISLAS = "No hay eventos para esta isla"
const val MENSAJE_CAMPO_VACIO = "No puede ser vacío"
const val MENSAJE_REGISTRO_NO_EXISTE = "No se puede borrar este registro, no existe"


const val LLAVE_TIPO_REPORTE_EVENTO = "TipoReporteEvento"
const val EVENTO_CONSULTA_STR = "EventoConsultaStr"

// BOTON DE PANICO
const val URL_REPORTE_BOTON_PANICO = URL_SERVIDOR + "user/panic_event"
const val PREFERENCIAS_BOTON_PANICO = "BotonPanico"
const val LLAVE_VIDEO = "LlaveVideo"
const val LLAVE_CONTACTOS = "LlaveContactos"
const val MAX_CONTACTOS = 7
const val MAX_DURACION_VIDEO = 120

// TESTIMONIO
const val TIPO_BOTON_EVENTO = "BotonTestimonio"
const val TESTIMONIO = "Testimonio"

const val DIRECCION_ANEXO = "DireccionAnexo"

// ISLAS
const val URL_CREA_ISLA = URL_SERVIDOR + "user/groups"   // post
const val URL_LISTA_MEMBERSHIPS = URL_SERVIDOR + "user/group_memberships"
const val URL_ACEPTA_INVITACION = URL_SERVIDOR + "user/group_memberships/:id"  // + :membership_id
const val ERROR_NO_EXISTE_MEMBERSHIP = 404
const val URL_BORRAR_MIEMBRO = URL_SERVIDOR + "user/groups/:id/members" // +/alias, DELETE
const val OK_BORRAR_MIEMBRO = 204
const val ERROR_BORRAR_MIEMBRO_NO_EXISTE = 404
const val ERROR_BORRAR_MIEMBRO_ACTIVO = 422

// ESTADO DE LA ISLA
const val LLAVE_NOMBRE_ISLA = "NombreIsla"
const val URL_LISTA_GRUPOS = URL_SERVIDOR + "user/groups"    // get
const val URL_AGREGAR_MIEMBRO = URL_SERVIDOR + "user/groups/:id/members" // + /alias, PUT
const val ERROR_MIEMBRO_YA_EXISTE = 422
const val ERROR_MIEMBRO_NO_EXISTE = 404

// PLAN DE SEGURIDAD
const val URL_LISTA_PLANES_DE_SEGURIDAD = URL_SERVIDOR + "user/groups/:id/security_plans" //GET
const val URL_ACTIVA_PLAN_SEGURIDAD = URL_SERVIDOR + "user/groups/:id/activate_security_plan"    // :id de la isla
const val URL_ASIGNAR_CENTRALIZADO = URL_SERVIDOR + "user/groups/:id/governance_model/centralized"
const val URL_CREA_PLAN_SEGURIDAD = URL_SERVIDOR + "user/groups/:id/security_plans"  // :id de la isla  POST
const val ERROR_PLAN_EXISTENTE = 422
const val URL_ACTUALIZA_PLAN_SEGURIDAD = URL_SERVIDOR +  "user/groups/:group_id/security_plans/:plan_id"  // PUT
const val ERROR_NO_EXISTE_PLAN = 404
const val URL_ASIGNAR_PESOS_PONDERADO = URL_SERVIDOR + "user/groups/:id/governance_model/weighted"  // PUT
const val URL_CAMBIA_MG_ESTABLE = URL_SERVIDOR + "user/groups/:id/security_plans/stable" // put

// ENCUESTAS
const val URL_LISTA_ENCUESTAS  = URL_SERVIDOR + "user/groups/:id/polls"     // get
const val URL_ENVIA_RESPUESTA = URL_SERVIDOR +  "user/polls/votes/"   // post
const val ERROR_VOTO_REPETIDO = 422
const val URL_CREA_ENCUESTA = URL_SERVIDOR + "user/polls"   // POST
const val URL_LEER_ISLA = URL_SERVIDOR + "user/groups/:id"    // GET

const val LLAVE_ARR_PESOS = "arrPesos"
const val LLAVE_ARR_ALIAS = "arrAlias"

// ROLES
const val D_MEMBERSHIP = "diccionarioMembership"
const val URL_TEMPLATE_ROLES = URL_SERVIDOR +  "user/groups/:id/security_plans/template"
const val URL_CREA_NUEVO_ROL = URL_SERVIDOR + "group_roles" // Post

// CONCEPTOS
const val PREFS_CONCEPTOS = "PrefsConceptos"
const val LLAVE_CONCEPTO_EXPLICA = "DescripcionIsla"
const val LLAVE_CONCEPTO_ISLA = "Isla"
const val LLAVE_SIGUIENTE_ACTIV = "IntentSiguiente"

const val LLAVE_CONCEPTO_ROL = "Roles"
const val LLAVE_CONCEPTO_MODELO_GOB = "ModeloGobernanza"


