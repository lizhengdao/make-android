package mx.rmr.make.islas

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONArrayRequestListener
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_roles_v_c.*
import mx.rmr.make.*
import mx.rmr.make.util.Comun
import mx.rmr.make.util.ConceptoActiv
import org.json.JSONArray
import org.json.JSONObject

class RolesActiv : AppCompatActivity(), ListenerListaRoles
{
    private var numeroEnvios: Int = 0
    private lateinit var arrMiembros: JSONArray
    private var idIsla: Int? = null
    private lateinit var alias: String
    private lateinit var dMembership: JSONObject

    // ROLES
    // Tabs
    private var arrAlias = mutableListOf<String>()
    // [(id, nombre)] para los tabs
    private var arrRoles = mutableListOf<Pair<Int,String>>()
    // Construye "alias":[ids_seleccionados], etc ]
    private var mapaAliasRoles: MutableMap<String, MutableList<Int>> = mutableMapOf()
    // Mapa de modelos de gobernanza ["alias":"democratic", ...]
    private var mapaAliasModelo: MutableMap<String, String> = mutableMapOf()
    // Mapa, guarda 'alias':planID para crear/actualizar plan de seguridad
    private var mapaAliasPlanId: MutableMap<String, Int> = mutableMapOf()

    private var creandoPlanSeguridad = true

    // Fragmento de la lista
    private lateinit var fragRoles: ListaRolesFrag

    private val NUEVO_ROL = "* Nuevo rol"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_roles_v_c)

        fragRoles = fragListaRoles as ListaRolesFrag

        val strJsonEvento = intent.extras?.getString(D_MEMBERSHIP) ?: "{}"
        dMembership = JSONObject(strJsonEvento)

        configurarIsla()

        if (resources.displayMetrics.widthPixels<500){
            rbDemocratico.textSize = 12f
            rbCentralizado.textSize = 12f
            rbPonderado.textSize = 12f
        }
    }

    private fun configurarIsla() {
        val dIsla = dMembership.getJSONObject("group") ?: return
        val nombreIsla = dIsla.getString("name") ?: return
        tvTituloIslaRoles.text = "Definiendo para: $nombreIsla"
        val idIsla = dIsla.getInt("id")
        descargarMiembrosIsla(idIsla)
    }

    private fun descargarMiembrosIsla(idIsla: Int) {
        pbEsperaRoles.visibility = View.VISIBLE

        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")
        alias = prefs.getString(LLAVE_ALIAS, "")!!

        val direccion = URL_TEMPLATE_ROLES.replace(":id", "$idIsla")

        AndroidNetworking.get(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    pbEsperaRoles.visibility = View.INVISIBLE
                    configurarTemplate(response)
                }

                override fun onError(anError: ANError?) {
                    pbEsperaRoles.visibility = View.INVISIBLE
                    when (anError?.errorCode) {
                        ERROR_NO_HAY_EVENTOS -> {
                            Comun.mostrarMensaje(
                                this@RolesActiv, MENSAJE_NO_ISLAS
                            )
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@RolesActiv, MENSAJE_NO_INTERNET
                            )
                        }
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@RolesActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@RolesActiv, MENSAJE_ERROR_GENERAL
                            )
                        }
                    }
                }
            })
    }

    private fun configurarTemplate(response: JSONObject?) {
        // Configura Tabs de alias
        arrMiembros = response?.getJSONArray("available_members")!!
        if (arrMiembros.length() == 0) {
            Comun.mostrarMensaje(
                this,
                "No puedes definir roles, porque los invitados no han confirmado su participación",
                true)
            return
        } else {
            idIsla = response.getInt("group_id")
            poblarTabsMiembros(arrMiembros)
            crearTablaRoles(response)
            cargarPlanSeguridad()       // Descarga Roles previos
            ponerListenerTabs()
        }
    }

    private fun ponerListenerTabs() {
        tabsAlias.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                val indice = tabsAlias.selectedTabPosition
                runOnUiThread {
                    actualizarFragmento(indice)
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }
        })
    }

    // Para los roles de cada alias
    private fun cargarPlanSeguridad() {
        pbEsperaRoles.visibility = View.VISIBLE

        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val direccion = URL_LISTA_PLANES_DE_SEGURIDAD.replace(":id", "${idIsla!!}")

        AndroidNetworking.get(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .build()
            .getAsJSONArray(object: JSONArrayRequestListener {
                override fun onResponse(response: JSONArray?) {
                    pbEsperaRoles.visibility = View.INVISIBLE
                    configurarRolesModelosGob(response!!)
                }

                override fun onError(anError: ANError?) {
                    pbEsperaRoles.visibility = View.INVISIBLE
                    when (anError?.errorCode) {
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@RolesActiv, MENSAJE_NO_INTERNET
                            )
                        }
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@RolesActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@RolesActiv, MENSAJE_ERROR_GENERAL
                            )
                        }
                    }
                }
            })
    }

    private fun configurarRolesModelosGob(response: JSONArray) {
        for (i in 0 until response.length()) {
            val dPlan = response.getJSONObject(i)
            val personaAusente = dPlan.getString("missing_person")
            if (personaAusente == alias || personaAusente=="null") {
                continue        // El mismo no se pone roles
            }
            // Planes id para crear/actualizar
            val idPlan = dPlan.getInt("id")
            mapaAliasPlanId[personaAusente] = idPlan

            val dRoles = dPlan.getJSONObject("members_roles")
            val arrRoles = mutableListOf<Int>()
            if (dRoles.has(alias)) {
                val arrRolesJSON = dRoles.getJSONArray(alias)
                for (k in 0 until arrRolesJSON.length()) {
                    val rol = arrRolesJSON.getJSONObject(k)
                    val idRol = rol.getInt("role_id")
                    arrRoles.add(idRol)
                }
            }
            mapaAliasRoles[personaAusente] = arrRoles
            if (arrRoles.isNotEmpty()) {
                creandoPlanSeguridad = false
            }
            // Modelo de gobernanza
            val dModelos = dPlan.getJSONObject("governance_details")
            val modeloGobernanza = dModelos.getString("winner")
            mapaAliasModelo[personaAusente] = modeloGobernanza
        }
        runOnUiThread {
            actualizarFragmento(0)
        }
    }

    private fun actualizarFragmento(i: Int) {
        val aliasFrag = arrAlias[i]
        val arrIdsRoles = mapaAliasRoles[aliasFrag]
        // Construir los índices
        val arrSeleccionados = mutableListOf<Int>()
        if (arrIdsRoles != null) {
            for ((indice, par) in arrRoles.withIndex()) {
                if (arrIdsRoles.contains(par.first)) {
                    arrSeleccionados.add(indice)
                }
            }
        }

        fragRoles.prender(arrSeleccionados)
        // Poner modelos
        when (mapaAliasModelo[aliasFrag]) {
            "centralized" -> {
                rbCentralizado.isChecked = true
            }
            "weighted" -> {
                rbPonderado.isChecked = true
            }
            else -> {
                rbDemocratico.isChecked = true
            }
        }
    }

    private fun crearTablaRoles(response: JSONObject?) {
        val arrRolesDisponibles = response?.getJSONArray("available_roles")

        for (i in 0 until arrRolesDisponibles!!.length()) {
            val dRol = arrRolesDisponibles.getJSONObject(i)
            val id = dRol.getInt("id")
            val nombre = dRol.getString("name")
            arrRoles.add(Pair(id, nombre))
        }
        arrRoles.add(Pair(0,NUEVO_ROL))     // Para agregar uno nuevo

        val frag = fragListaRoles as ListaRolesFrag
        frag.cambiarAdaptador(arrRoles)
    }

    private fun poblarTabsMiembros(arrMiembros: JSONArray) {
        tabsAlias.removeAllTabs()
        for (i in 0 until arrMiembros.length()) {
            val alias = arrMiembros.getString(i)
            val tab = tabsAlias.newTab()
            tab.text = alias
            tabsAlias.addTab(tab)
            arrAlias.add(alias)
        }
    }

    override fun clickOnIndex(posicion: Int, seleccionado: Boolean) {
        val indiceAlias = tabsAlias.selectedTabPosition
        val aliasTab = arrAlias[indiceAlias]
        var arrSeleccionados = mapaAliasRoles[aliasTab]!!

        val idRol = arrRoles[posicion].first
        val nombreRol = arrRoles[posicion].second
        if (nombreRol==NUEVO_ROL) {
            agregarNuevoRol()
            val frag = fragListaRoles as ListaRolesFrag
            frag.apagar(posicion)
        } else {
            if (seleccionado) {
                arrSeleccionados.add(idRol)
            } else {
                arrSeleccionados.remove(idRol)
            }
        }
    }

    private fun agregarNuevoRol() {
        val etNombreRol = EditText(this)
        val dialogo = AlertDialog.Builder(this)
            .setTitle("Agrega un nuevo rol")
            .setMessage("Teclea el nombre:\n")
            .setView(etNombreRol)
            .setPositiveButton("Aceptar", DialogInterface.OnClickListener() {
                    _: DialogInterface, _: Int ->
                val nuevo = etNombreRol.text.toString()
                subirCreaNuevoRol(nuevo)
            })
            .setNegativeButton("Cancelar", null)
            .setCancelable(false)
            .create()
        dialogo.show()
    }

    private fun subirCreaNuevoRol(nuevoRol: String) {
        pbEsperaRoles.visibility = View.VISIBLE
        // Token
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val direccion = URL_CREA_NUEVO_ROL

        var jsonBody = JSONObject(
            """
            {
            "name": "$nuevoRol",
            "group": $idIsla
            }
            """.trimIndent()
        )

        AndroidNetworking.post(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .addJSONObjectBody(jsonBody)
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    pbEsperaRoles.visibility = View.INVISIBLE
                    Comun.mostrarMensaje(this@RolesActiv, "Se creó el nuevo rol con éxito")
                    configurarIsla()
                }

                override fun onError(anError: ANError?) {
                    pbEsperaRoles.visibility = View.INVISIBLE
                    when (anError?.errorCode) {
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@RolesActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@RolesActiv, MENSAJE_NO_INTERNET, true)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@RolesActiv, MENSAJE_ERROR_GENERAL, true)
                        }
                    }
                }
            })
    }

    fun cambiarModeloGob(v: View) {
        val indiceAlias = tabsAlias.selectedTabPosition
        val aliasTab = arrAlias[indiceAlias]
        mapaAliasModelo[aliasTab] = v.contentDescription.toString()
    }

    fun grabarPlanesSeguridad(v: View) {
        if (estanListosPlanes()) {
            enviarPlanesSeguridad()
        } else {
            Comun.mostrarMensaje(this, "Debes seleccionar roles para todos los miembros de la isla")
        }
    }

    private fun enviarPlanesSeguridad() {
        // Obtener roles y modelo de gob para cada alias
        numeroEnvios = mapaAliasRoles.size

        for ((alias,arrRoles) in mapaAliasRoles) {
            val modeloGob = mapaAliasModelo[alias]
            if (creandoPlanSeguridad) {
                // CREAR
                val body = JSONObject("""
                { "group": $idIsla,
                "missing_person": "$alias",
                "roles": $arrRoles,
                "governance_model": "$modeloGob"
                }
                """.trimIndent())
                // Enviar este body
                subirCreaPlan(body, alias)
            } else {
                // ACTUALIZAR
                val body = JSONObject("""
                {
                "roles": $arrRoles,
                "governance_model": "$modeloGob"
                }
                """.trimIndent())
                // Enviar este body con el planID correcto
                subirActualizarPlan(body, mapaAliasPlanId[alias])
            }
        }
    }

    private fun subirActualizarPlan(body: JSONObject, planId: Int?) {
        pbEsperaRoles.visibility = View.VISIBLE
        // Token
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val urlDireccion = URL_ACTUALIZA_PLAN_SEGURIDAD.replace(":group_id", "$idIsla")
            .replace(":plan_id", "$planId")

        AndroidNetworking.put(urlDireccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .addJSONObjectBody(body)
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    pbEsperaRoles.visibility = View.INVISIBLE
                    numeroEnvios--
                    if (numeroEnvios<=0) {
                        Comun.mostrarMensaje(this@RolesActiv, "Los planes de seguridad se actualizaron con éxito", true)
                    }
                }

                override fun onError(anError: ANError?) {
                    pbEsperaRoles.visibility = View.INVISIBLE
                    when (anError?.errorCode) {
                        ERROR_NO_EXISTE_PLAN -> {
                            val json = anError.response as JSONObject
                            Comun.mostrarMensaje(this@RolesActiv, "Error: ${json["message"]}")
                        }
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@RolesActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@RolesActiv, MENSAJE_NO_INTERNET, true)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@RolesActiv, MENSAJE_ERROR_GENERAL, true)
                        }
                    }
                }
            })
    }

    private fun subirCreaPlan(body: JSONObject, alias: String) {
        pbEsperaRoles.visibility = View.VISIBLE
        // Token
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val urlDireccion = URL_CREA_PLAN_SEGURIDAD.replace(":id", "$idIsla")

        AndroidNetworking.post(urlDireccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .addJSONObjectBody(body)
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    pbEsperaRoles.visibility = View.INVISIBLE
                    numeroEnvios--
                    if (numeroEnvios<=0) {
                        Comun.mostrarMensaje(this@RolesActiv, "Los planes de seguridad se crearon con éxito", true)
                    }
                }

                override fun onError(anError: ANError?) {
                    pbEsperaRoles.visibility = View.INVISIBLE
                    when (anError?.errorCode) {
                        ERROR_PLAN_EXISTENTE -> {
                            val json = anError.response as JSONObject
                            //println("Error: ${json["message"]}")
                            Comun.mostrarMensaje(this@RolesActiv, "Error: ${json["message"]}")
                        }
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@RolesActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@RolesActiv, MENSAJE_NO_INTERNET, true)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@RolesActiv, MENSAJE_ERROR_GENERAL, true)
                        }
                    }
                }
            })
    }

    private fun estanListosPlanes(): Boolean {
        // Si mapa de alias-roles tiene un arreglo vacío, no puede enviar
        for (par in mapaAliasRoles) {
            if (par.value.isEmpty()) {
                return false
            }
        }
        return true
    }

    // Mostrar el concepro de Modeo de Gobernanza
    fun cambiarModeloGobernanza(v: View) {
        if (! yaConcedio(LLAVE_CONCEPTO_MODELO_GOB)) {
            val intConcepto = Intent(this, ConceptoActiv::class.java)
            intConcepto.putExtra(LLAVE_CONCEPTO_EXPLICA, LLAVE_CONCEPTO_MODELO_GOB)
            startActivity(intConcepto)
        }
    }

    private fun yaConcedio(concepto: String): Boolean {
        val prefs = getSharedPreferences(PREFS_CONCEPTOS, Context.MODE_PRIVATE)
        return prefs.getBoolean(concepto, false)
    }
}
