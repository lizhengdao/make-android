package mx.rmr.make.islas

import android.content.Context
import android.view.View
import android.widget.AbsListView
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.core.view.get
import androidx.fragment.app.ListFragment
import mx.rmr.make.R

import mx.rmr.make.botonpanico.ListenerFragmentoLista

/**
 * Lista de Alias para crear la isla
 */
class ListaCadenasFrag : ListFragment()
{
    // Para quitar la selección
    private var ultimoRenglon: View? = null
    var indiceSeleccionado: Int? = null

    private val arrDatos = mutableListOf<String>()
    var contexto: Context? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.contexto = context
    }

    fun cambiarAdaptador(arrAlias: MutableList<String>?) {
        arrDatos.clear()

        if (arrAlias!=null) {
            for (alias in arrAlias) {
                arrDatos.add(alias)
            }

            val adapter = ArrayAdapter(
                contexto!!, android.R.layout.simple_list_item_1,
                arrDatos)

            listAdapter = adapter
        }

        listView.setOnItemLongClickListener { parent, view, position, id ->
            val listener = contexto as ListenerFragmentoLista
            listener.longClickOnIndex(position, this@ListaCadenasFrag)

            // Apagar selección
            ultimoRenglon?.setBackgroundResource(android.R.color.transparent)
            ultimoRenglon = null

            true
        }
    }

    override fun onListItemClick(l: ListView, v: View, position: Int, id: Long) {
        super.onListItemClick(l, v, position, id)

        ultimoRenglon?.setBackgroundResource(android.R.color.transparent)

        v.setBackgroundResource(R.color.azul_make_light)
        val listener = contexto as ListenerFragmentoLista
        listener.clickOnIndex(position, this)
        ultimoRenglon = v

        indiceSeleccionado = position
    }

}
