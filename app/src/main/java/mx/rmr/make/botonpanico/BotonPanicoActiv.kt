package mx.rmr.make.botonpanico

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Location
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.MediaStore
import android.provider.Settings
import android.telephony.SmsManager
import android.text.method.ScrollingMovementMethod
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.ListFragment
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_boton_panico.*
import mx.rmr.make.*
import mx.rmr.make.eventos.ReporteEventoActiv
import mx.rmr.make.gps.GPS
import mx.rmr.make.gps.GPSListener
import mx.rmr.make.util.Comun
import org.json.JSONObject
import java.io.File
import java.util.*
import kotlin.concurrent.fixedRateTimer
import kotlin.concurrent.timerTask

/**
 * Botón de pánico y como caso especial Testimonio
 */
class BotonPanicoActiv : AppCompatActivity(), ListenerFragmentoLista, GPSListener
{
    // Tipo de botón (Botón de pánico,testimonio)
    private var tipoReporte: TipoReporte = TipoReporte.BOTON_PANICO

    // Contactos
    private val CODIGO_LEE_CONTACTO = 300
    var arrContactos: MutableList<Contacto>? = null

    // GPS y dirección de texto
    private val CODIGO_PERMISO_GPS: Int = 200
    private lateinit var gps: GPS
    private var posicionActual: Location? = null
    private var direccionCompleta: Address? = null

    // ANEXOS
    private val CODIGO_PERMISO_EXTERNO: Int = 201
    private var idReportePanico: Int? = null
    private var idVideoAnexo: Int? = null

    // SMS
    private val CODIGO_PERMISO_SMS = 410

    // VIDEO
    private val CODIGO_GRABA_VIDEO = 500
    private var pathVideoComplemento: String? = null

    // TRAZAS
    private val NUMERO_MAX_TRAZAS = 7
    private var timerTrazas: Timer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_boton_panico)

        tvConsola.movementMethod = ScrollingMovementMethod()

        tvConsola.visibility = View.GONE
        btnCancelar.visibility = View.GONE
        pbEsperaPanico.visibility = View.GONE
        vistaInferiorPanico.visibility = View.VISIBLE

        recuperarTiposEvento()
        recuperarPreferenciaVideo()
        recuperarListaContactos()

        val tipo = intent.getStringExtra(TIPO_BOTON_EVENTO)
        if (tipo!=null && tipo == TESTIMONIO) {
            tipoReporte = TipoReporte.BOTON_TESTOMONIO
            tvTituloBtnPanico.text = "TESTIMONIO"
            btnEnviarReportePanico.text = "Enviar evento en curso"
            checkPrefVideo.isChecked = true
            checkPrefVideo.isClickable = false

            // Botón de testimonio, solo muestra la consola
            // Esconder vista de contactos y reemplazarla por la consola
            vistaInferiorPanico.visibility = View.GONE
            tvConsola.visibility = View.VISIBLE
        }
    }

    override fun onStart() {
        super.onStart()

        AndroidNetworking.initialize(this)
        configurarGPS()
    }

    override fun onStop() {
        if (this::gps.isInitialized) {
            gps.detenerActualizaciones()
        }
        AndroidNetworking.cancelAll()

        super.onStop()
    }

    private fun configurarGPS() {
        gps = GPS()
        gps.gpsListener = this
        gps.inicializar(this)

        if (verificarPermisos()) {
            iniciarActualizacionesPosicion()
            if (! verificarPermisosSMS()) {
                pedirPermisosSMS()
            } else {
                tienePermisosExternos()
            }
        } else {
            pedirPermisos()
        }
    }

    private fun verificarPermisosSMS(): Boolean {
        val estadoPermiso = ActivityCompat.checkSelfPermission(this,
            Manifest.permission.SEND_SMS)
        return estadoPermiso == PackageManager.PERMISSION_GRANTED
    }

    private fun pedirPermisosSMS() {
        val requiereJustificacion = ActivityCompat.shouldShowRequestPermissionRationale(this,
            Manifest.permission.SEND_SMS)

        if (requiereJustificacion) {
            ActivityCompat.requestPermissions(this@BotonPanicoActiv, arrayOf(Manifest.permission.SEND_SMS),
                CODIGO_PERMISO_SMS)
        } else {
            ActivityCompat.requestPermissions(this@BotonPanicoActiv, arrayOf(Manifest.permission.SEND_SMS),
                CODIGO_PERMISO_SMS)
        }
    }

    // Botón enviar Evento en curso
    fun enviarReportePanico(v: View) {
        btnEnviarReportePanico.isEnabled = false

        // Esconder vista de contactos y reemplazarla por la consola
        vistaInferiorPanico.visibility = View.GONE
        tvConsola.visibility = View.VISIBLE
        pbEsperaPanico.visibility = View.VISIBLE
        btnCancelar.visibility = View.VISIBLE

        if (tipoReporte==TipoReporte.BOTON_PANICO) {
            tvConsola.text = "\nGenerando reporte de panico\n"
            // Enviar reporte de pánico -> Grabar video -> Enviar anexos, por 7 s.
            subirReportePanico()
            // Enviar SMS
            enviarSMS()
        } else {
            tvConsola.text = "\nGenerando reporte de Evento en curso\n"
            grabarVideo()
        }
    }

    // Envía SMS a los contactos
    private fun enviarSMS() {
        // Pedir permisos
        if (! verificarPermisosSMS()) {
            pedirPermisosSMS()
        }

        agregarMensajeConsola("\nEnviando mensajes SMS")
        // Obtener contactos
        var dirContactos = ""
        // Obtiene, de las preferencias, la lista de contactos
        val preferencias = getSharedPreferences(PREFERENCIAS_BOTON_PANICO, Context.MODE_PRIVATE)
        if (preferencias.contains(LLAVE_CONTACTOS) && verificarPermisosSMS()){
            val strContactos = preferencias.getStringSet(LLAVE_CONTACTOS, null)
            if (strContactos!=null && strContactos.size>0) {
                for (strContacto in strContactos) {
                    val datos = strContacto.split("&-&")
                    val telefono = datos[1]
                    dirContactos += "$telefono;"
                }
                dirContactos = dirContactos.substringBeforeLast(";")

                val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
                val usuario = prefs.getString(LLAVE_ALIAS, "Desconocido")
                val tipoEvento = "${spTipoEventoPanico.selectedItem}"
                val urlPosicion = obtenerUrlPosicion()
                val direccion = direccionCompleta?.getAddressLine(0) ?: "Desconocida"
                val mensajeSMS = "$urlPosicion, Emergencia de $usuario " +
                        "($tipoEvento), $direccion"
                // Usar SMSManager y un thread
                enviarTareaSMS(dirContactos, mensajeSMS)
            } else {
                tvConsola.append("\nNo hay contactos/permisos para enviar SMS")
            }
        } else {
            tvConsola.append("\nNo hay contactos/permisos para enviar SMS")
        }
    }

    // Usa un Intent para grabar el video 120 segundos máximo
    private fun grabarVideo() {
        if (checkPrefVideo.isChecked) {
            // Grabar video -> Trazas
            tvConsola.append("\nGrabando video")
            val intGrabaVideo = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
            intGrabaVideo.putExtra(MediaStore.EXTRA_DURATION_LIMIT, MAX_DURACION_VIDEO)
            if (intGrabaVideo.resolveActivity(packageManager)!=null) {
                startActivityForResult(intGrabaVideo, CODIGO_GRABA_VIDEO)
            } else {
                agregarMensajeConsola("\nTu dispositivo no puede grabar video")
                if (tipoReporte==TipoReporte.BOTON_PANICO) {
                    enviarInfoAdicional(null)
                } else {
                    agregarMensajeConsola("\n\nNo puedes enviar Evento en curso")
                    resetearGUI()
                }
            }
        } else {
            if (tipoReporte==TipoReporte.BOTON_PANICO) {
                enviarInfoAdicional(null)
            } else {
                agregarMensajeConsola("\n\nNo puedes enviar Evento en curso")
                resetearGUI()
            }
        }
    }

    private fun enviarTareaSMS(dirContactos: String, mensajeSMS: String) {
        val numeros = dirContactos.split(";")
        val smsManager = SmsManager.getDefault()
        val t = Thread(Runnable {
                for (numero in numeros) {
                    val partes = smsManager.divideMessage(mensajeSMS)
                    try {
                        smsManager.sendMultipartTextMessage(numero,
                            null, partes, null, null
                        )
                    } catch (e: Exception) {
                        println(e.message)
                        agregarMensajeConsola("\nError SMS, ${e.message}")
                    }

                }
                agregarMensajeConsola("\nTermina de enviar SMS\n")
        })
        t.start()
    }

    private fun obtenerUrlPosicion(): String {
        var latitud = 0.0
        var longitud = 0.0
        if (posicionActual != null) {
            latitud = posicionActual!!.latitude
            longitud = posicionActual!!.longitude
        }
        return "https://www.google.com/maps/search/?api=1&query=$latitud,$longitud"
    }

    // Envía el reporte de pánico (INICIAL), después video -> complementos
    private fun subirReportePanico() {
        val categoria = spTipoEventoPanico.selectedItem as String
        val fecha = obtenerFecha()
        val hora = obtenerHora()
        var latitud = "0"
        var longitud = "0"
        if (posicionActual != null) {
            latitud = "${posicionActual!!.latitude}"
            longitud = "${posicionActual!!.longitude}"
        }
        var pais ="Desconocido"
        var estado = "Desconocido"
        var municipio = "Desconocido"
        var direccion = "Desconocida"
        var cp = "0"
        if (direccionCompleta!=null) {
            val dDireccion = direccionCompleta!!
            pais = dDireccion.countryName
            estado = dDireccion.adminArea
            municipio = dDireccion.locality
            cp = dDireccion.postalCode
            direccion = dDireccion.getAddressLine(0)
        }
        var groupId = ""
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        if (prefs.contains(LLAVE_ID_ISLA_SELECCIONADA)) {
            val idIsla = prefs.getInt(LLAVE_ID_ISLA_SELECCIONADA, 0)
            groupId = """
                ,
                'group_id': '$idIsla'
            """.trimIndent()
        }

        val jsonBody = JSONObject("""{
            'category': '$categoria',
            'date' : '$fecha',
            'time' : '$hora',
            'location_latitude' : $latitud,
            'location_longitude' : $longitud,
            'location_address' : '$direccion',
            'location_state' : '$estado',
            'location_city' : '$municipio',
            'location_country' : '$pais',
            'location_zip_code' : '$cp'
            $groupId
            }
        """.trimIndent())

        // Token
        val token = prefs.getString(LLAVE_TOKEN, "")

        val urlDireccion = URL_REPORTE_BOTON_PANICO
        AndroidNetworking.post(urlDireccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .addJSONObjectBody(jsonBody)
            .setPriority(Priority.HIGH)
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    idReportePanico = response?.getInt("id")   // Para complementos
                    tvConsola.append("\nSe ha enviado con éxito el reporte inicial de pánico al servidor\n")
                    // TERMINA, concurrente está enviando SMS
                    grabarVideo()   // -> complementos
                }

                override fun onError(anError: ANError?) {
                    when (anError?.errorCode) {
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@BotonPanicoActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        ERROR_NO_INTERNET -> {
                            agregarMensajeConsola("\n\n$MENSAJE_NO_INTERNET\n")
                        }
                        else -> {
                            agregarMensajeConsola("\n\n$MENSAJE_ERROR_GENERAL\n")
                        }
                    }
                    cancelarReportePanico(btnCancelar)
                }
            })
    }

    private fun obtenerFecha(): String {
        val calendario = Calendar.getInstance()
        // Valores iniciales (hora/fecha del sistema)
        return String.format("%d-%02d-%02d", calendario.get(Calendar.YEAR),
            calendario.get(Calendar.MONTH)+1, calendario.get(Calendar.DAY_OF_MONTH))
    }

    private fun obtenerHora(): String {
        val calendario = Calendar.getInstance()
        return String.format("%d:%02d", calendario.get(Calendar.HOUR_OF_DAY),
            calendario.get(Calendar.MINUTE))
    }

    private fun recuperarListaContactos() {
        arrContactos = mutableListOf()  // Inicia vacía
        // Obtiene, de las preferencias, la lista de contactos
        val preferencias = getSharedPreferences(PREFERENCIAS_BOTON_PANICO, Context.MODE_PRIVATE)
        if (preferencias.contains(LLAVE_CONTACTOS)) {
            val strContactos = preferencias.getStringSet(LLAVE_CONTACTOS, null)
            for (strContacto in strContactos!!) {
                val datos = strContacto.split("&-&")
                val nombre = datos[0]
                val telefono = datos[1]
                arrContactos?.add(Contacto(nombre, telefono))
            }
        }
        actualizarListaFragmento()
    }

    private fun recuperarPreferenciaVideo() {
        val preferencias = getSharedPreferences(PREFERENCIAS_BOTON_PANICO, Context.MODE_PRIVATE)
        checkPrefVideo.isChecked = preferencias.getBoolean(LLAVE_VIDEO, false)
    }

    private fun recuperarTiposEvento() {
        val prefs = getSharedPreferences(PREFS_EVENTOS, Context.MODE_PRIVATE)
        val tiposEvento = prefs.getStringSet(LLAVE_EVENTOS, mutableSetOf<String>())!!
        var arrTipoEventoSpinner: MutableList<String>?
        if (tiposEvento.isNotEmpty()) {
            arrTipoEventoSpinner = tiposEvento.toMutableList()
        } else {
            // Constantes
            arrTipoEventoSpinner = mutableListOf("Robo", "Secuestro", "Intento de secuestro",
                "Asalto", "Violación", "Tortura", "Extorsión")
        }

        val arrTipos = arrTipoEventoSpinner
        agregarAdaptador(arrTipos.toList())
    }

    private fun agregarAdaptador(listaTipos: List<String>) {
        // Poblar el spinner
        val adaptador = ArrayAdapter(
            this, android.R.layout.simple_spinner_item,
            listaTipos.toMutableList()
        )
        spTipoEventoPanico.adapter = adaptador
    }

    fun cambiarPreferenciaVideo(v: View) {
        val preferencias = getSharedPreferences(PREFERENCIAS_BOTON_PANICO, Context.MODE_PRIVATE).edit()
        preferencias.putBoolean(LLAVE_VIDEO, checkPrefVideo.isChecked)
        preferencias.apply()
    }

    // Contactos
    fun agregarContacto(v: View) {
        if (arrContactos!=null && arrContactos!!.size >= MAX_CONTACTOS) {
            Comun.mostrarMensaje(this, "Solo puedes agregar $MAX_CONTACTOS contactos")
            return
        }
        val intContacto = Intent(Intent.ACTION_PICK)
        intContacto.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
        if (intContacto.resolveActivity(packageManager)!=null) {
            startActivityForResult(intContacto, CODIGO_LEE_CONTACTO)
        } else {
            Comun.mostrarMensaje(this, "No puedes acceder a los contactos")
        }
    }

    private fun actualizarListaFragmento() {
        // Obtener el fragmento
        val fragContactos =
            supportFragmentManager.findFragmentById(R.id.fragListaContactos) as ListaContactosFrag
        fragContactos.cambiarAdaptador(arrContactos)
        guardarContactosPreferencias(arrContactos)
        // Actualizar num. contactos
        tvContactosNum.text = "Contactos para SMS (${arrContactos?.size}/$MAX_CONTACTOS)"

    }

    private fun guardarContactosPreferencias(arrContactos: MutableList<Contacto>?) {
        var setContactos = mutableSetOf<String>()
        if (arrContactos!=null) {
            for (contacto in arrContactos) {
                setContactos.add("${contacto.nombre}&-&${contacto.telefono}")
            }
            val preferencias = getSharedPreferences(PREFERENCIAS_BOTON_PANICO, Context.MODE_PRIVATE).edit()
            preferencias.putStringSet(LLAVE_CONTACTOS, setContactos)
            preferencias.apply()
        }
    }

    // Borrar contacto
    override fun longClickOnIndex(posicion: Int, fragmento: ListFragment?) {
        val dialogo = AlertDialog.Builder(this)
            .setMessage("¿Quiere borrar este contacto?")
            .setNegativeButton("Si") { _, _ ->
                arrContactos?.removeAt(posicion)
                actualizarListaFragmento()
            }
            .setPositiveButton("No",null)
            .create()
        dialogo.show()
    }


    // GPS
    override fun actualizarPosicion(posicion: Location) {
        imgGPS.setImageResource(R.drawable.check)
        posicionActual = posicion
        // Obtener direccion en texto
        val descarga = object : ReporteEventoActiv.Companion.DescargaDireccion(this) {
            override fun onPostExecute(result: Address?) {
                super.onPostExecute(result)
                // NO DETIENE ACTUALIZACIONES (BOTON DE PANICO)
                if (result != null) {
                    direccionCompleta = result
                    imgDireccion.setImageResource(R.drawable.check)
                    imgRed.setImageResource(R.drawable.check)
                } else {
                    descargarDireccionBigData(posicion)
                }
            }
        }
        descarga.execute(posicionActual)
    }

    private fun descargarDireccionBigData(posicion: Location) {
        val direccion = "https://api.bigdatacloud.net/data/reverse-geocode-client?latitude=${posicion.latitude}&longitude=${posicion.longitude}&localityLanguage=es"
        AndroidNetworking.get(direccion)
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    val pais = response?.getString("countryName")
                    val estado = response?.getString("principalSubdivision")
                    val municipio = response?.getString("locality")
                    var cp = "0"
                    if (response?.has("postcode")!!) {
                        cp = response?.getString("postcode")
                    }
                    direccionCompleta = Address(Locale.getDefault())
                    direccionCompleta?.countryName = pais
                    direccionCompleta?.adminArea = estado
                    direccionCompleta?.locality = municipio
                    direccionCompleta?.postalCode = cp
                    direccionCompleta?.setAddressLine(0, "$municipio, $estado, $pais")
                    imgDireccion.setImageResource(R.drawable.check)
                    imgRed.setImageResource(R.drawable.check)
                }

                override fun onError(anError: ANError?) {
                    direccionCompleta = Address(Locale.getDefault())
                    direccionCompleta?.countryName = "No disponible"
                    direccionCompleta?.adminArea = "No disponible"
                    direccionCompleta?.locality = "No disponible"
                    direccionCompleta?.postalCode = "0"
                    direccionCompleta?.setAddressLine(0, "Desconocida")
                }
            })
    }

    // GPS
    private fun verificarPermisos(): Boolean {
        val estadoPermiso = ActivityCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_FINE_LOCATION)
        return estadoPermiso == PackageManager.PERMISSION_GRANTED
    }

    private fun iniciarActualizacionesPosicion() {
        gps.iniciarActualizaciones()
    }

    private fun pedirPermisos() {
        val requiereJustificacion = ActivityCompat.shouldShowRequestPermissionRationale(this,
            Manifest.permission.ACCESS_FINE_LOCATION)

        if (requiereJustificacion) {
            mostrarDialogo()
        } else {
            ActivityCompat.requestPermissions(this@BotonPanicoActiv, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                CODIGO_PERMISO_GPS)
        }
    }

    private fun mostrarDialogo() {
        val dialogo = AlertDialog.Builder(this)
        dialogo.setMessage("Necesitas GPS para esta app.")
            .setPositiveButton("Aceptar", DialogInterface.OnClickListener { dialog, which ->
                ActivityCompat.requestPermissions(this@BotonPanicoActiv, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    CODIGO_PERMISO_GPS)
            })
            .setNeutralButton("Cancelar", null)
        dialogo.show()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<out String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == CODIGO_PERMISO_GPS) {
            if (grantResults.isEmpty()) {
            } else if (grantResults.first() == PackageManager.PERMISSION_GRANTED) {
                // ¡Hay permiso!
                gps.iniciarActualizaciones()
                if (! verificarPermisosSMS()) {
                    pedirPermisosSMS()
                }
            } else {
                // Permiso negado
                val dialogo = AlertDialog.Builder(this)
                dialogo.setMessage("Esta app requiere GPS, ¿Quieres configurar el permiso?")
                    .setPositiveButton("Aceptar", DialogInterface.OnClickListener { dialog, which ->
                        val intent = Intent()
                        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                        val uri = Uri.fromParts("package",
                            BuildConfig.APPLICATION_ID, null)
                        intent.data = uri
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                    })
                    .setNeutralButton("Cancelar", DialogInterface.OnClickListener { dialog, which ->
                        // CERRAR ACTIVIDAD
                        println("No hay forma de usar gps, cerrar la actividad")
                        finish()
                    })
                    .setCancelable(false)
                dialogo.show()
            }
        } else if (requestCode == CODIGO_PERMISO_EXTERNO) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this@BotonPanicoActiv, "Permisos aceptados", Toast.LENGTH_SHORT).show()
            }
        } else if (requestCode == CODIGO_PERMISO_SMS) {
            println("Permiso de SMS!!!")
            tienePermisosExternos()
        }
        else {
            println("No puede leer los archivos de medios: $requestCode")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            GPS.CODIGO_PRENDE_GPS -> when (resultCode) {
                Activity.RESULT_OK ->
                    pedirPermisos()
                Activity.RESULT_CANCELED -> {
                    // CERRAR ACTIVIDAD
                    finish()
                }
            }
            CODIGO_LEE_CONTACTO -> {
                if (resultCode == Activity.RESULT_OK) {
                    val contactoUri: Uri = data?.data ?: return
                    val projection = arrayOf(
                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                        ContactsContract.CommonDataKinds.Phone.NUMBER
                    )
                    val cursor = contentResolver.query(
                        contactoUri, projection,
                        null, null, null
                    )
                    if (cursor != null && cursor.moveToFirst()) {
                        val indiceNombre =
                            cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
                        val indiceTelefono =
                            cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                        val nombre = cursor.getString(indiceNombre)
                        val telefono = cursor.getString(indiceTelefono)
                        // Agrega a la lista de contactos este valor y refresca el fragmento
                        val nuevoContacto = Contacto(nombre, telefono)
                        arrContactos?.add(nuevoContacto)
                        actualizarListaFragmento()
                    }
                    cursor?.close()
                }
            }
            CODIGO_GRABA_VIDEO -> {
                val uriVideoSel = data?.data
                if (resultCode == Activity.RESULT_OK) {
                    tvConsola.append("\nVideo capturado\n")
                } else {
                    tvConsola.append("\nVideo cancelado\n")
                }
                if (tipoReporte==TipoReporte.BOTON_PANICO) {
                    enviarInfoAdicional(uriVideoSel)
                } else {
                    enviarReporteTestimonio(uriVideoSel)
                }
            }
        }
    }

    // Manda video anexo -> reporte
    private fun enviarReporteTestimonio(uriVideoSel: Uri?) {
        if (uriVideoSel==null) {
            agregarMensajeConsola("\nReporte de evento en curso terminado")
            resetearGUI()
            return
        }
        pathVideoComplemento = getRealPathVideoFromURI(uriVideoSel!!)
        subirAnexo(pathVideoComplemento)    // -> enviar reporte
    }

    // Manda 7 trazas, una cada minuto. La primera lleva como anexo el video
    private fun enviarInfoAdicional(uriVideoSel: Uri?) {
        if (uriVideoSel!=null) {
            pathVideoComplemento = getRealPathVideoFromURI(uriVideoSel!!)
            subirAnexo(pathVideoComplemento)    // -> 7 trazas
        } else {
            tvConsola.append("\n\nEnviando 7 trazas, una cada minuto")
            // 7 trazas
            subirTrazas()
        }
    }

    private fun subirTrazas() {
        var tiempoInicial = 0L
        if (idReportePanico==null) {
            tiempoInicial = 7*1000; // Espera a que termine de enviarse
        }
        var numeroTraza = 0
        if (idVideoAnexo!=null) {
            numeroTraza += 1
            agregarMensajeConsola("\n\nEnviando traza $numeroTraza/$NUMERO_MAX_TRAZAS")
            subirComplementoPanico()
            tiempoInicial = 60*1000
        }
        // Timer para 7(6) trazas
        timerTrazas = fixedRateTimer("timerTrazas", false,
            tiempoInicial, 1000*60) {
            numeroTraza++
            agregarMensajeConsola("\n\nEnviando traza $numeroTraza/$NUMERO_MAX_TRAZAS")
            subirComplementoPanico()
            if (numeroTraza==NUMERO_MAX_TRAZAS) {
                Timer().schedule( timerTask {
                    agregarMensajeConsola("\n\nTermina de enviar evento de pánico")
                }, 2000)
                gps?.detenerActualizaciones()
                resetearGUI()
                cancel()
            }
        }
    }

    fun cancelarReportePanico(v: View) {
        agregarMensajeConsola("\n\nReporte cancelado")
        resetearGUI()
    }

    private fun resetearGUI() {
        runOnUiThread {
            pbEsperaPanico.visibility = View.INVISIBLE
            btnCancelar.visibility = View.INVISIBLE
            timerTrazas?.cancel()
            btnEnviarReportePanico.isEnabled = true
        }
    }

    private fun subirComplementoPanico() {
        agregarMensajeConsola("\nEnviando complemento de reporte de pánico")
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val descripcion = "Complemento, botón de pánico\n\nPosición:\n${obtenerUrlPosicion()}\n"
        val fecha = obtenerFecha()
        val hora = obtenerHora()

        var listaAnexos = mutableListOf<Int>()
        if (idVideoAnexo!=null) {
            listaAnexos.add(idVideoAnexo!!)
            idVideoAnexo = null
        }

        // Ya se validaron los campos
        val jsonBody = JSONObject("""{
            'incident': $idReportePanico,
            'title' : 'Complemento, botón de pánico',
            'description' : '$descripcion',
            'date' : '$fecha',
            'time' : '$hora',
            'attachments' : $listaAnexos
            }
        """.trimIndent())

        //println("Complemento: $jsonBody")

        val direccion = URL_CREA_EVENTO_COMPLEMENTO
            .replace(":id","$idReportePanico")

        AndroidNetworking.post(direccion)
            .addHeaders("Content-type","application/json")
            .addHeaders("Authorization", "Bearer $token")
            .addJSONObjectBody(jsonBody)
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    agregarMensajeConsola("\nComplemento enviado con éxito")
                }

                override fun onError(anError: ANError?) {
                    when (anError?.errorCode) {
                        ERROR_CREA_EVENTO -> {
                            val jsonError = anError.errorBody as JSONObject
                            val mensaje = jsonError.getString("message")
                           agregarMensajeConsola("\n$mensaje")
                        }
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@BotonPanicoActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        ERROR_NO_INTERNET -> {
                            agregarMensajeConsola("\n\n$MENSAJE_NO_INTERNET\n")
                        }
                        else -> {
                            agregarMensajeConsola("\n\n$MENSAJE_ERROR_GENERAL\n")
                        }
                    }
                }
            })
    }

    private fun subirAnexo(videoPath: String?) {
        agregarMensajeConsola("\nSubiendo video")
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val videoFile = File(videoPath)
        val tipoVideo = videoPath?.substring(videoPath?.lastIndexOf(".") + 1)!!

        val direccion = URL_CREA_ATTACHMENT

        AndroidNetworking.upload(direccion)
            .addHeaders("Content-type", "application/x-www-form-urlencoded")
            .addHeaders("Authorization", "Bearer $token")
            .addMultipartFile("file", videoFile)
            .addMultipartParameter("filename", "video.$tipoVideo")
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    val idAnexo = response?.getInt("id")
                    if (idAnexo != null) {
                        agregarMensajeConsola("\nVideo entregado al servidor")
                        idVideoAnexo = idAnexo
                        if (tipoReporte==TipoReporte.BOTON_PANICO) {
                            subirTrazas()
                        } else {
                            subirReporteTestimonio()
                        }
                    }
                }

                override fun onError(anError: ANError?) {
                    when (anError?.errorCode) {
                        ERROR_EXT_INVALIDA -> {
                            val jsonError = anError.errorBody as JSONObject
                            val mensaje = jsonError.getString("message")
                            agregarMensajeConsola("\n$mensaje")
                        }
                        ERROR_TAM_INCORRECTO -> {
                            val jsonError = anError.errorBody as JSONObject
                            val arrError = jsonError.getJSONArray("file")
                            val mensaje = arrError[0] as String
                            agregarMensajeConsola("\n$mensaje")
                        }
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@BotonPanicoActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true
                            )
                        }
                        ERROR_NO_INTERNET -> {
                            agregarMensajeConsola("\n$MENSAJE_NO_INTERNET")
                        }
                        else -> {
                            agregarMensajeConsola("\n$MENSAJE_ERROR_GENERAL")
                        }
                    }
                }

            })
    }

    // Envía el reporte de testimonio -> termina
    private fun subirReporteTestimonio() {
        val categoria = spTipoEventoPanico.selectedItem as String
        val descripcion = "Evento en curso\n\nPosición:\n${obtenerUrlPosicion()}\n"
        val fecha = obtenerFecha()
        val hora = obtenerHora()
        var latitud = "0"
        var longitud = "0"
        if (posicionActual != null) {
            latitud = "${posicionActual!!.latitude}"
            longitud = "${posicionActual!!.longitude}"
        }
        var pais = "Desconocido"
        var estado = "Desconocido"
        var municipio = "Desconocido"
        var direccion = "Desconocida"
        var cp = "0"
        if (direccionCompleta != null) {
            val dDireccion = direccionCompleta!!
            pais = dDireccion.countryName
            estado = dDireccion.adminArea
            municipio = dDireccion.locality
            cp = dDireccion.postalCode
            direccion = dDireccion.getAddressLine(0)
        }

        var listaAnexos = mutableListOf<Int>()
        if (idVideoAnexo!=null) {
            listaAnexos.add(idVideoAnexo!!)
            idVideoAnexo = null
        }

        var groupId = ""
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        if (prefs.contains(LLAVE_ID_ISLA_SELECCIONADA)) {
            val idIsla = prefs.getInt(LLAVE_ID_ISLA_SELECCIONADA, 0)
            groupId = """
                ,
                'group_id': $idIsla
            """.trimIndent()
        }

        val jsonBody = JSONObject(
            """{
            'title': 'Evento en curso',
            'category': '$categoria',
            'testimony': true,
            'description': '$descripcion',
            'date' : '$fecha',
            'time' : '$hora',
            'location_latitude' : $latitud,
            'location_longitude' : $longitud,
            'location_address' : '$direccion',
            'location_state' : '$estado',
            'location_city' : '$municipio',
            'location_country' : '$pais',
            'location_zip_code' : '$cp',
            'attachments' : $listaAnexos$groupId
            }
        """.trimIndent()
        )

        // Token
        val token = prefs.getString(LLAVE_TOKEN, "")

        val urlDireccion = URL_REPORTE_BOTON_PANICO
        AndroidNetworking.post(urlDireccion)
            .addHeaders("Content-type", "application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .addJSONObjectBody(jsonBody)
            .setPriority(Priority.HIGH)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    agregarMensajeConsola("\n\nSe ha enviado con éxito el reporte de evento en curso")
                    agregarMensajeConsola("\n\nTermina envío de Evento en curso")
                    resetearGUI()
                }

                override fun onError(anError: ANError?) {
                    when (anError?.errorCode) {
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@BotonPanicoActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true
                            )
                        }
                        ERROR_NO_INTERNET -> {
                            agregarMensajeConsola("\n\n$MENSAJE_NO_INTERNET\n")
                        }
                        else -> {
                            agregarMensajeConsola("\n\n$MENSAJE_ERROR_GENERAL\n")
                        }
                    }
                    cancelarReportePanico(btnCancelar)
                }
            })
    }

    private fun agregarMensajeConsola(mensaje: String) {
        runOnUiThread {
            tvConsola.append(mensaje)
        }
    }

    private fun tienePermisosExternos(): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true
        }

        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
            && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true
        }

        if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
            || shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Snackbar.make(
                tvConsola, "Los permisos son necesarios para poder usar la aplicación",
                Snackbar.LENGTH_INDEFINITE
            ).setAction(android.R.string.ok, View.OnClickListener {
                requestPermissions(
                    arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ), CODIGO_PERMISO_EXTERNO
                )
            }).show()
        } else {
            requestPermissions(
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), CODIGO_PERMISO_EXTERNO
            )
        }

        return false
    }

    private fun getRealPathVideoFromURI(uri: Uri): String? {
        uri.let { returnUri ->
            contentResolver.query(returnUri, null, null, null, null)
        }?.use { cursor ->
            val path = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            cursor.moveToFirst()
            return cursor.getString(path)
        }
        return null
    }

    enum class TipoReporte
    {
        BOTON_PANICO,
        BOTON_TESTOMONIO
    }
}
