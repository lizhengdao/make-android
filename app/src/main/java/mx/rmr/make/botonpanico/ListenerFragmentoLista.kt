package mx.rmr.make.botonpanico

import androidx.fragment.app.ListFragment

interface ListenerFragmentoLista
{
    // Opcional
    fun clickOnIndex(posicion: Int, fragmento: ListFragment? = null) {

    }
    // Opcional
    fun longClickOnIndex(posicion: Int, fragmento: ListFragment? = null) {

    }
}