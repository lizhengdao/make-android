package mx.rmr.make.menu

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import kotlinx.android.synthetic.main.activity_menu_plan.*
import mx.rmr.make.*
import mx.rmr.make.encuestas.EncuestasActiv
import mx.rmr.make.eventos.EventosActiv
import mx.rmr.make.islas.CreaIslaActiv
import mx.rmr.make.islas.IslasActiv
import mx.rmr.make.plan.EstadoIslaActiv
import mx.rmr.make.util.Comun
import mx.rmr.make.util.ConceptoActiv
import org.json.JSONArray
import org.json.JSONObject

class MenuPlanActiv : AppCompatActivity()
{
    var idIslaSeleccionada: Int = 0
    var nombreIsla: String? = null
    var dIslaSeleccionada: JSONObject? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_plan)

        // Leer la última isla seleccionada
        cargarIslaSeleccionada()
    }

    private fun cargarIslaSeleccionada() {
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val strIsla = prefs.getString(LLAVE_JSON_ISLA, "{}")
        val dIsla = JSONObject(strIsla)
        if (dIsla.has("name")) {
            nombreIsla = dIsla.getString("name")
            tvIslaSeleccionada.text = nombreIsla
            idIslaSeleccionada = dIsla.getInt("id")
            dIslaSeleccionada = dIsla
        } else {
            tvIslaSeleccionada.text = "No seleccionada"
            idIslaSeleccionada = ISLA_NO_SELECCIONADA;
        }
    }

    fun mostrarEventos(v: View) {
        if (nombreIsla != null) {
            val intEventos = Intent(this, EventosActiv::class.java)
            startActivity(intEventos)
        } else {
            Comun.mostrarMensaje(this, "Primero selecciona una isla")
        }
    }

    fun mostrarCreaIsla(v: View) {

        if (yaConcedio(LLAVE_CONCEPTO_ISLA)) {
            val intCreaIsla = Intent(this, CreaIslaActiv::class.java)
            startActivity(intCreaIsla)
        } else {
            val intConcepto = Intent(this, ConceptoActiv::class.java)
            intConcepto.putExtra(LLAVE_CONCEPTO_EXPLICA, LLAVE_CONCEPTO_ISLA)

            val intCreaIsla = Intent(this, CreaIslaActiv::class.java)
            intConcepto.putExtra(LLAVE_SIGUIENTE_ACTIV, intCreaIsla)

            startActivity(intConcepto)
        }
    }

    fun mostrarMisIslas(v: View) {
        val intMisIslas = Intent(this, IslasActiv::class.java)
        startActivity(intMisIslas)
    }

    // Selecciona una de las islas de este alias
    fun seleccionarIsla(v: View) {
        // Recupera el alias
        val preferencias = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE) ?: return
        val alias = preferencias.getString(LLAVE_ALIAS, NO_EXISTE)
        val token = preferencias.getString(LLAVE_TOKEN, NO_EXISTE)

        // Descargar islas y dejarle seleccionar la que quiere trabajar
        descargarIslasMenu(alias, token)
    }

    private fun descargarIslasMenu(alias: String?, token: String?) {
        //pbEsperaPlan.visibility = View.VISIBLE
        habilitarEntrada(false)

        val direccion = URL_LISTA_MEMBERSHIPS

        AndroidNetworking.get(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    //pbEsperaPlan.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    crearMenuIslas(response?.getJSONArray("active"))
                }

                override fun onError(anError: ANError?) {
                    //pbEsperaPlan.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    if (anError != null) {
                        when (anError.errorCode) {
                            ERROR_INFO_INVALIDA -> {
                                Comun.mostrarMensaje(
                                    this@MenuPlanActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                            }
                            ERROR_NO_INTERNET -> {
                                Comun.mostrarMensaje(
                                    this@MenuPlanActiv, MENSAJE_NO_INTERNET)
                            }
                            else -> {
                                Comun.mostrarMensaje(
                                    this@MenuPlanActiv, MENSAJE_ERROR_GENERAL)
                            }
                        }
                    }
                }
            })
    }

    private fun crearMenuIslas(arrIslas: JSONArray?) {
        if (arrIslas != null) {
            val arrIslaAceptada = ArrayList<Pair<String, Int>>()
            for (i in 0 until arrIslas.length()) {
                val dMembresia = arrIslas.get(i) as JSONObject
                val dIsla = dMembresia.getJSONObject("group")
                nombreIsla = dIsla.getString("name")
                idIslaSeleccionada = dIsla.getInt("id")
                arrIslaAceptada.add(Pair(nombreIsla!!, idIslaSeleccionada!!))
            }
            mostrarMenuIslas(arrIslaAceptada, arrIslas)
        } else {
            Comun.mostrarMensaje(this, "No tienes islas", false)
        }
    }

    private fun mostrarMenuIslas(
        arrIslaAceptada: ArrayList<Pair<String, Int>>, arrIslas: JSONArray) {
        val arrNombres = Array(arrIslaAceptada.size)
                                    { i -> arrIslaAceptada[i].first}

        var indiceSeleccionado = 0
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Selecciona una isla")
            .setSingleChoiceItems(arrNombres, 0
            ) { _, which ->
                indiceSeleccionado = which
            }
            .setPositiveButton("Aceptar") { _, _ ->
                tvIslaSeleccionada.text = arrNombres[indiceSeleccionado]
                val datosIsla = arrIslas.getJSONObject(indiceSeleccionado)
                dIslaSeleccionada = datosIsla.getJSONObject("group")
                nombreIsla = dIslaSeleccionada!!.getString("name")
                idIslaSeleccionada = dIslaSeleccionada!!.getInt("id")
                val preferencias = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
                preferencias.edit().apply {
                    putInt(LLAVE_ID_ISLA_SELECCIONADA, arrIslaAceptada[indiceSeleccionado].second)
                    putString(LLAVE_JSON_ISLA, dIslaSeleccionada.toString())
                    commit()
                }
            }
            .setNegativeButton("Cancelar", null)

        builder.create()
        builder.show()
    }

    fun mostrarEstadoIsla(v: View) {
        if (nombreIsla != null) {
            val intEstadoIsla = Intent(this, EstadoIslaActiv::class.java)
            // Pasar idIsla, nombreIsla
            intEstadoIsla.putExtra(LLAVE_NOMBRE_ISLA, nombreIsla)
            intEstadoIsla.putExtra(LLAVE_ID_ISLA_SELECCIONADA, idIslaSeleccionada)
            intEstadoIsla.putExtra(LLAVE_JSON_ISLA, dIslaSeleccionada.toString())
            startActivity(intEstadoIsla)
        } else {
            Comun.mostrarMensaje(this, "Primero selecciona una isla")
        }
    }

    private fun yaConcedio(concepto: String): Boolean {
        val prefs = getSharedPreferences(PREFS_CONCEPTOS, Context.MODE_PRIVATE)
        return prefs.getBoolean(concepto, false)
    }

    fun mostrarEncuestas(v: View) {
        if (nombreIsla!=null) {
            val intEncuestas = Intent(this, EncuestasActiv::class.java)
            startActivity(intEncuestas)
        } else {
            Comun.mostrarMensaje(this, "Primero selecciona una isla")
        }
    }

    private var entradaHabilitada = true

    private fun habilitarEntrada(habilitada: Boolean) {
        pbEsperaPlan.visibility = if (habilitada) View.INVISIBLE else View.VISIBLE
        entradaHabilitada = habilitada
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        return if (entradaHabilitada) super.dispatchTouchEvent(ev) else true
    }
}
