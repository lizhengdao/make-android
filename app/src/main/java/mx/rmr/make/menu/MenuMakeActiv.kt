package mx.rmr.make.menu

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import mx.rmr.make.*
import mx.rmr.make.botonpanico.BotonPanicoActiv
import mx.rmr.make.login.LoginActiv

/**
 * Menú inicial, Plan de seguridad / Testimonio
 */
class MenuMakeActiv : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_make)
    }

    fun cerrarSesion(v: View) {
        val intLogin = Intent(this, LoginActiv::class.java)
        startActivity(intLogin)
        finish()
    }

    fun mostrarPlanSeguridad(v: View) {
        val intPlan = Intent(this, MenuPlanActiv::class.java)
        startActivity(intPlan)
    }

    fun mostrarTestimonio(v: View) {
        val intTestimonio = Intent(this, BotonPanicoActiv::class.java)
        intTestimonio.putExtra(TIPO_BOTON_EVENTO, TESTIMONIO)
        startActivity(intTestimonio)
    }
}
