package mx.rmr.make.eventos

import android.Manifest.permission
import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.*
import android.content.pm.PackageManager
import android.content.res.TypedArray
import android.database.Cursor
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.text.InputType
import android.view.MotionEvent
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONArrayRequestListener
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_reporte_evento.*
import mx.rmr.make.*
import mx.rmr.make.gps.GPS
import mx.rmr.make.gps.GPSListener
import mx.rmr.make.util.Comun
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class ReporteEventoActiv : AppCompatActivity(), GPSListener
{
    private val CODIGO_PERMISO_EXTERNO: Int = 201
    private var arrTipoEventoSpinner: MutableList<String>? = null

    // Para obtener la direccion en texto
    private val CODIGO_PERMISO_GPS: Int = 200
    private lateinit var gps: GPS
    private var posicon_actual: Location? = null

    // Para los pickers de estados y municipios
    private lateinit var arrEstados: Array<String>
    private lateinit var arrIdsEstados: TypedArray
    private var indiceSeleccionado = 0

    // Anexos
    private var arrUrlFotos = mutableListOf<Uri>()
    private var arrUrlFotosBak = mutableListOf<Uri>()   // Temporal para subir/cancelar
    private var urlVideo: Uri? = null
    private var urlAudio: Uri? = null
    private val CODIGO_IMAGEN = 250
    private val CODIGO_VIDEO = 252
    private val CODIGO_AUDIO = 254
    private val MAX_FOTOS_ANEXO = 4

    private var arrIdsAnexos = mutableListOf<Int>()

    // Complemento
    private var tipoEvento = TipoEvento.CONSULTA
    private var idEvento = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reporte_evento)

        AndroidNetworking.initialize(this)

        arrTipoEventoSpinner = mutableListOf()

        tipoEvento = intent.extras?.get(LLAVE_TIPO_REPORTE_EVENTO) as TipoEvento

        if (tipoEvento==TipoEvento.CAPTURA) {
            prepararCaptura()
        } else if (tipoEvento==TipoEvento.CONSULTA) {
            val strJsonEvento = intent.extras?.getString(EVENTO_CONSULTA_STR)
            val dEvento = JSONObject(strJsonEvento)
            prepararConsulta(dEvento)
        } else if (tipoEvento==TipoEvento.COMPLEMENTO) {
            val strJsonEvento = intent.extras?.getString(EVENTO_CONSULTA_STR)
            val dEvento = JSONObject(strJsonEvento)
            prepararComplemento(dEvento)
        }
    }

    private fun prepararComplemento(dEvento: JSONObject) {

        configurarFecha()

        tvTituloReporte.text = "Complemento de evento"
        layoutCapturaAnexos.visibility = View.VISIBLE
        layoutConsultaAnexos.visibility = View.GONE

        //etNombreCorto.setText(dEvento.getString("title"))
        //etDescripcion.setText(dEvento.getString("description"))
        etFecha.setText(dEvento.getString("date"))
        etHora.setText(dEvento.getString("time"))
        etPais.setText(dEvento.getString("location_country"))
        etEstado.setText(dEvento.getString("location_state"))
        etMunicipio.setText(dEvento.getString("location_city"))
        etCodigoPostal.setText(dEvento.getString("location_zip_code"))
        etDireccion.setText(dEvento.getString("location_address"))
        val arrTipos = listOf<String>(dEvento.getString("category"))

        //etNombreCorto.isEnabled = false
        //etDescripcion.isEnabled = false
        spTipoEvento.isEnabled = false
        //etFecha.isEnabled = false
        //etHora.isEnabled = false
        etPais.isEnabled = false
        etEstado.isEnabled = false
        etMunicipio.isEnabled = false
        etCodigoPostal.isEnabled = false
        etDireccion.isEnabled = false
        btnSpinEstado.isEnabled = false
        btnSpinMunicipio.isEnabled = false

        agregarAdaptador(arrTipos)

        btnEnviarReporte.text = "Enviar complemento"

        idEvento = dEvento.getInt("id")
    }

    private fun prepararCaptura() {
        AndroidNetworking.initialize(this)
        descargarTiposEventos()
        configurarFecha()
        configurarGPS()
        configurarListaMunicipios()
        tvTituloReporte.text = "Captura nuevo evento"
        layoutCapturaAnexos.visibility = View.VISIBLE
        layoutConsultaAnexos.visibility = View.GONE

        if (verificarPermisos()) {
            iniciarActualizacionesPosicion()
        } else {
            pedirPermisos()
        }
    }

    private fun prepararConsulta(dEvento: JSONObject) {
        tvTituloReporte.text = "Consulta evento"

        layoutCapturaAnexos.visibility = View.GONE
        layoutConsultaAnexos.visibility = View.VISIBLE

        etNombreCorto.setText(dEvento.getString("title"))
        etNombreCorto.isFocusable = false

        val strDescripcion = dEvento.getString("description")
        //val descripcion = URLDecoder.decode(strDescripcion, "UTF-8")
        etDescripcion.setText(strDescripcion)
        etFecha.setText(dEvento.getString("date"))
        etHora.setText(dEvento.getString("time"))
        etPais.setText(dEvento.getString("location_country"))
        etEstado.setText(dEvento.getString("location_state"))
        etMunicipio.setText(dEvento.getString("location_city"))
        etCodigoPostal.setText(dEvento.getString("location_zip_code"))
        etDireccion.setText(dEvento.getString("location_address"))
        val arrTipos = listOf<String>(dEvento.getString("category"))

        etNombreCorto.isEnabled = false

        etDescripcion.inputType = InputType.TYPE_NULL
        etDescripcion.isSingleLine = false
        etDescripcion.isClickable = true

        spTipoEvento.isEnabled = false
        etFecha.isEnabled = false
        etHora.isEnabled = false
        etPais.isEnabled = false
        etEstado.isEnabled = false
        etMunicipio.isEnabled = false
        etCodigoPostal.isEnabled = false
        etDireccion.isEnabled = false
        btnSpinEstado.isEnabled = false
        btnSpinMunicipio.isEnabled = false

        agregarAdaptador(arrTipos)

        crearMenuAnexos(dEvento)
    }

    private fun crearMenuAnexos(dEvento: JSONObject) {
        // Botones para anexos
        val arrAnexos = dEvento.getJSONArray("attachments")
        if (arrAnexos.length()==0) {
            tvTituloListaAnexos.setText("Este reporte no tiene anexos.")
        }
        var numImagen = 0

        for (i in 0 until arrAnexos.length()) {
            val dAnexo = arrAnexos[i] as JSONObject
            val direccion = dAnexo.getString("file_url")
            val archivo = dAnexo.getString("file")
            if (archivo.endsWith("jpg", true)
                || archivo.endsWith("jpeg", true)
                || archivo.endsWith("png", true)
            ) {
                val btnImagen = Button(this)
                btnImagen.setBackgroundResource(R.color.azul_make)
                layoutConsultaAnexos.addView(btnImagen)
                btnImagen.text = "Imagen $numImagen"
                numImagen++
                btnImagen.setOnClickListener {
                    cargarVisualizadorAnexo(direccion)
                }
            } else if (archivo.endsWith(".mp4")
                || archivo.endsWith(".mov")
            ) {
                val btnVideo = Button(this)
                btnVideo.setBackgroundResource(R.color.azul_make)
                layoutConsultaAnexos.addView(btnVideo)
                btnVideo.text = "Video"
                btnVideo.setOnClickListener {
                    cargarVisualizadorAnexo(direccion)
                }
            } else if (archivo.endsWith(".mp3")
                || archivo.endsWith(".m4a")
            ) {
                val btnAudio = Button(this)
                btnAudio.setBackgroundResource(R.color.azul_make)
                layoutConsultaAnexos.addView(btnAudio)
                btnAudio.text = "Audio"
                btnAudio.setOnClickListener {
                    cargarVisualizadorAnexo(direccion)
                }
            }
        }
    }

    private fun cargarVisualizadorAnexo(direccion: String) {
        val intAnexo = Intent(this, VisualizaAnexo::class.java)
        intAnexo.putExtra(DIRECCION_ANEXO, direccion)
        startActivity(intAnexo)
    }

    private fun configurarListaMunicipios() {

        arrEstados = resources.getStringArray(R.array.estados)
        arrIdsEstados = resources.obtainTypedArray(R.array.arrEstados)
    }

    fun mostrarListaEstados(v: View) {
        val dialogo = AlertDialog.Builder(this)
        dialogo.setTitle("Selecciona el estado")
            .setItems(arrEstados) {
                _, indice ->
                indiceSeleccionado = indice
                etEstado.setText(arrEstados[indiceSeleccionado])
            }
            .setNeutralButton("Cancelar", null)
            .create()
            .show()
    }

    fun mostrarListaMunicipios(v: View) {
        val idEstado = arrIdsEstados.getResourceId(indiceSeleccionado, 0)
        val arrMunicipios = resources.getStringArray(idEstado)

        val dialogo = AlertDialog.Builder(this)
        dialogo.setTitle("Selecciona el municipio")
            .setItems(arrMunicipios) {
                    _, indice ->
                etMunicipio.setText(arrMunicipios[indice])
            }
            .setNeutralButton("Cancelar", null)
            .create()
            .show()
    }

    override fun onStop() {
        if (this::gps.isInitialized) {
            gps.detenerActualizaciones()
        }
        super.onStop()
    }

    private fun configurarGPS() {
        gps = GPS()
        gps.gpsListener = this
        gps.inicializar(this)
    }

    private fun descargarTiposEventos() {

        habilitarEntrada(false)

        val direccion = URL_TIPOS_INCIDENTES

        AndroidNetworking.get(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .build()
            .getAsJSONArray(object: JSONArrayRequestListener {
                override fun onResponse(response: JSONArray?) {
                    //pbEsperaEvento.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    crearArregloTipoEventos(response)
                }

                override fun onError(anError: ANError?) {
                    //pbEsperaEvento.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    // Intenta utilizar el arreglo local
                    recuperarTiposEvento()
                }
            })
    }

    private fun recuperarTiposEvento() {
        val prefs = getSharedPreferences(PREFS_EVENTOS, Context.MODE_PRIVATE)
        val tiposEvento = prefs.getStringSet(LLAVE_EVENTOS, mutableSetOf<String>())!!

        arrTipoEventoSpinner?.clear()
        if (tiposEvento.isNotEmpty()) {
            arrTipoEventoSpinner = tiposEvento.toMutableList()
        } else {
            // Constantes
            arrTipoEventoSpinner = mutableListOf("Robo", "Secuestro", "Intento de secuestro",
            "Asalto", "Violación", "Tortura", "Extorsión")
        }

        val arrTipos = arrTipoEventoSpinner!!
        agregarAdaptador(arrTipos.toList())
    }

    private fun crearArregloTipoEventos(arrEventos: JSONArray?) {
        arrTipoEventoSpinner?.clear()
        for (i in 0 until arrEventos!!.length()) {
            val dTipo = arrEventos?.get(i) as JSONObject
            val tipoEvento = dTipo.getString("description")
            arrTipoEventoSpinner?.add(tipoEvento)
        }
        agregarAdaptador(arrTipoEventoSpinner?.toList()!!)

        // Guardar en preferencias los tipos
        val conjuntoEventos = arrTipoEventoSpinner?.toMutableSet()!!
        var prefs = getSharedPreferences(PREFS_EVENTOS, Context.MODE_PRIVATE)
        prefs.edit().apply {
            putStringSet(LLAVE_EVENTOS, conjuntoEventos)
            commit()
        }
    }

    private fun agregarAdaptador(listaTipos: List<String>) {
        // Poblar el spinner
        val adaptador = ArrayAdapter<String>(
            this, android.R.layout.simple_spinner_item,
            listaTipos.toMutableList()
        )
        spTipoEvento.adapter = adaptador
    }

    private fun configurarFecha() {
        val calendario = Calendar.getInstance()
        // Valores iniciales (hora/fecha del sistema)
        val fechaFormato = String.format("%d-%02d-%02d", calendario.get(Calendar.YEAR),
            calendario.get(Calendar.MONTH)+1, calendario.get(Calendar.DAY_OF_MONTH))
        etFecha.setText(fechaFormato)

        val horaFormato = String.format("%d:%02d", calendario.get(Calendar.HOUR_OF_DAY),
            calendario.get(Calendar.MINUTE))
        etHora.setText(horaFormato)

        // Selecciona fecha
        val dateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            calendario.set(Calendar.YEAR, year)
            calendario.set(Calendar.MONTH, monthOfYear)
            calendario.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val formato = "yyyy-MM-dd"
            val sdf = SimpleDateFormat(formato, Locale.ENGLISH)
            etFecha.setText(sdf.format(calendario.time))
        }

        etFecha.setOnClickListener {
            DatePickerDialog(this@ReporteEventoActiv, dateSetListener,
                calendario.get(Calendar.YEAR),
                calendario.get(Calendar.MONTH),
                calendario.get(Calendar.DAY_OF_MONTH)).show()
        }

        // Selecciona hora
        val horaListener = TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
            calendario.set(Calendar.HOUR_OF_DAY, hourOfDay)
            calendario.set(Calendar.MINUTE, minute)

            val formato = "H:mm"
            val stf = SimpleDateFormat(formato, Locale.ENGLISH)
            etHora.setText(stf.format(calendario.time))
        }

        etHora.setOnClickListener {
            TimePickerDialog(this@ReporteEventoActiv, horaListener,
                calendario.get(Calendar.HOUR_OF_DAY),
                calendario.get(Calendar.MINUTE), true
                ).show()
        }
    }

    // GPS
    private fun verificarPermisos(): Boolean {
        val estadoPermiso = ActivityCompat.checkSelfPermission(this,
            permission.ACCESS_FINE_LOCATION)
        return estadoPermiso == PackageManager.PERMISSION_GRANTED
    }

    private fun iniciarActualizacionesPosicion() {
        gps.iniciarActualizaciones()
    }

    private fun pedirPermisos() {
        val requiereJustificacion = ActivityCompat.shouldShowRequestPermissionRationale(this,
            permission.ACCESS_FINE_LOCATION)

        if (requiereJustificacion) {
            mostrarDialogo()
        } else {
            ActivityCompat.requestPermissions(this@ReporteEventoActiv, arrayOf(permission.ACCESS_FINE_LOCATION),
                CODIGO_PERMISO_GPS)
        }
    }

    private fun mostrarDialogo() {
        val dialogo = AlertDialog.Builder(this)
        dialogo.setMessage("Necesitas GPS para esta app.")
            .setPositiveButton("Aceptar") { dialog, which ->
                ActivityCompat.requestPermissions(this@ReporteEventoActiv, arrayOf(permission.ACCESS_FINE_LOCATION),
                    CODIGO_PERMISO_GPS)
            }
            .setNeutralButton("Cancelar", null)
        dialogo.show()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<out String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == CODIGO_PERMISO_GPS) {
            if (grantResults.isEmpty()) {
            } else if (grantResults.first() == PackageManager.PERMISSION_GRANTED) {
                // ¡Hay permiso!
                gps.iniciarActualizaciones()
            } else {
                // Permiso negado
                val dialogo = AlertDialog.Builder(this)
                dialogo.setMessage("Esta app requiere GPS, ¿Quieres configurar el permiso?")
                    .setPositiveButton("Aceptar", DialogInterface.OnClickListener { dialog, which ->
                        val intent = Intent()
                        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                        val uri = Uri.fromParts("package",
                            BuildConfig.APPLICATION_ID, null)
                        intent.data = uri
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                    })
                    .setNeutralButton("Cancelar", DialogInterface.OnClickListener { dialog, which ->
                        // CERRAR ACTIVIDAD
                        println("No hay forma de usar gps, cerrar la actividad")
                        finish()
                    })
                    .setCancelable(false)
                dialogo.show()
            }
        } else if (requestCode == CODIGO_PERMISO_EXTERNO) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this@ReporteEventoActiv, "Permisos aceptados", Toast.LENGTH_SHORT).show()
                mostrarOpciones()
            }
        } else {
            finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            GPS.CODIGO_PRENDE_GPS -> when (resultCode) {
                Activity.RESULT_OK ->
                    pedirPermisos()
                Activity.RESULT_CANCELED -> {
                    // CERRAR ACTIVIDAD
                    finish()
                }
            }
            CODIGO_IMAGEN -> {
                if (resultCode == Activity.RESULT_OK) {
                    val uriImagen = data?.data
                    arrUrlFotos.add(uriImagen!!)
                    actualizarListaAnexos()
                }
            }
            CODIGO_VIDEO -> {
                if (resultCode == Activity.RESULT_OK) {
                    val uriVideoSel = data?.data
                    urlVideo = uriVideoSel
                    actualizarListaAnexos()
                }
            }
            CODIGO_AUDIO -> {
                if (resultCode == Activity.RESULT_OK) {
                    val uriAudioSel = data?.data
                    urlAudio = uriAudioSel
                    actualizarListaAnexos()
                }
            }
        }
    }

    private fun actualizarListaAnexos() {
        tvAnexos.text = ""
        if (arrUrlFotos.size>0) {
            var numFoto = 1
            for (uri in arrUrlFotos) {
                tvAnexos.append("Imagen $numFoto \n")
                numFoto++
            }
        }
        if (urlVideo != null) {
            tvAnexos.append("Video\n")
        }
        if (urlAudio != null) {
            tvAnexos.append("Audio\n")
        }
    }

    fun borrarAnexos(v: View) {
        arrUrlFotos.clear()
        urlVideo = null
        urlAudio = null
        tvAnexos.setText("No hay anexos seleccionados")
    }

    override fun actualizarPosicion(posicion: Location) {
        // Nueva posicion, pedir dirección
        posicon_actual = posicion

        val descarga = object: DescargaDireccion(this) {
            override fun onPostExecute(result: Address?) {
                super.onPostExecute(result)
                gps.detenerActualizaciones()

                if (result!=null) {
                    etPais.setText(result.countryName)
                    etEstado.setText(result.adminArea)
                    if (result.locality != null) {
                        etMunicipio.setText(result.locality)
                    } else {
                        etMunicipio.setText("Desconocido")
                    }
                    etCodigoPostal.setText(result.postalCode)
                    etDireccion.setText(result.getAddressLine(0))
                    calcularIndiceSeleccionado()
                } else {
                    etDireccion.setText("Desconocida")
                    descargarDireccionBigData(posicion)
                }
            }
        }
        descarga.execute(posicion)
    }

    private fun calcularIndiceSeleccionado() {
        val estado = etEstado.text.toString()
        for (k in arrEstados.indices) {
            if (arrEstados[k].contains(estado, true)) {
                indiceSeleccionado = k
                break
            }
        }
    }


    private fun descargarDireccionBigData(posicion: Location) {
        val direccion = "https://api.bigdatacloud.net/data/reverse-geocode-client?latitude=${posicion.latitude}&longitude=${posicion.longitude}&localityLanguage=es"
        AndroidNetworking.get(direccion)
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    val pais = response?.getString("countryName")
                    val estado = response?.getString("principalSubdivision")
                    val municipio = response?.getString("locality") ?: "Desconocido"
                    etPais.setText(pais)
                    etEstado.setText(estado)
                    etMunicipio.setText(municipio)
                    var cp = "0"
                    if (response?.has("postcode")!!) {
                        cp = response?.getString("postcode")

                    }
                    etCodigoPostal.setText(cp)
                    etDireccion.setText("$municipio, $estado, $pais")
                }

                override fun onError(anError: ANError?) {
                    etDireccion.setText("Desconocida")
                }
            })
    }

    // ANEXOS
    fun seleccionarAnexos(v: View) {
        if (tienePermisosExternos()) {
            mostrarOpciones()
        }
    }

    private fun tienePermisosExternos(): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true
        }

        if (checkSelfPermission(permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
            && checkSelfPermission(permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true
        }

        if (shouldShowRequestPermissionRationale(permission.READ_EXTERNAL_STORAGE)
            || shouldShowRequestPermissionRationale(permission.WRITE_EXTERNAL_STORAGE)) {
            Snackbar.make(
                tvAnexos, "Los permisos son necesarios para poder usar la aplicación",
                Snackbar.LENGTH_INDEFINITE
            ).setAction(android.R.string.ok, View.OnClickListener {
                requestPermissions(
                    arrayOf(
                        permission.READ_EXTERNAL_STORAGE, permission.WRITE_EXTERNAL_STORAGE
                    ), CODIGO_PERMISO_EXTERNO
                )
            }).show()
        } else {
            requestPermissions(
                arrayOf(
                    permission.READ_EXTERNAL_STORAGE, permission.WRITE_EXTERNAL_STORAGE
                ), CODIGO_PERMISO_EXTERNO
            )
        }

        return false
    }

    private fun mostrarOpciones() {
        val opciones = arrayOf("Fotos", "Video", "Audio")
        val alerta = AlertDialog.Builder(this)
        alerta.setTitle("Tipo de anexo")
            .setItems(opciones, DialogInterface.OnClickListener { dialog, which ->
                when (which) {
                    0 -> {
                        if (arrUrlFotos.size>=MAX_FOTOS_ANEXO) {
                            Comun.mostrarMensaje(this, "Solo puedes agregar 4 imágenes.")
                        } else {
                            cargarFotos()
                        }
                    }
                    1 -> {
                        if (urlVideo == null) {
                            cargarVideo()
                        } else {
                            Comun.mostrarMensaje(this, "Solo puedes agregar un video.")
                        }

                    }
                    2 -> {
                        if (urlAudio == null) {
                            cargarAudio()
                        } else {
                            Comun.mostrarMensaje(this, "Solo puedes agregar un audio.")
                        }
                    }
                    else -> {
                        dialog.dismiss()
                    }
                }
            })
            .setNeutralButton("Cancelar", DialogInterface.OnClickListener { dialog, which ->
                dialog.dismiss()
            })
            .create()
            .show()
    }

    private fun cargarAudio() {
        val audioIntent = Intent()
        audioIntent.type = "audio/*"
        audioIntent.action = Intent.ACTION_GET_CONTENT
        audioIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
        startActivityForResult(audioIntent, CODIGO_AUDIO)
    }

    private fun cargarVideo() {
        val intFotos = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        intFotos.setType("video/*")
        startActivityForResult(intFotos, CODIGO_VIDEO)
    }

    private fun cargarFotos() {
        val intFotos = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intFotos.setType("image/*")
        startActivityForResult(intFotos, CODIGO_IMAGEN)
    }

    fun enviarReporte(v: View) {
        if (checarCampos()) {
            // Enviar anexos primero; al terminar, envía el reporte
            subirAnexos()
        }
    }

    private fun checarCampos(): Boolean {
        var resultado = true
        if (etNombreCorto.text?.isEmpty()!!) {
            resultado = false
            etNombreCorto.error = MENSAJE_CAMPO_VACIO
        }
        if (etDescripcion.text?.isEmpty()!!) {
            resultado = false
            etDescripcion.error = MENSAJE_CAMPO_VACIO
        }
        if (etPais.text?.isEmpty()!!) {
            resultado = false
            etPais.error = MENSAJE_CAMPO_VACIO
        }
        if (etEstado.text?.isEmpty()!!) {
            resultado = false
            etEstado.error = MENSAJE_CAMPO_VACIO
        }
        if (etMunicipio.text?.isEmpty()!!) {
            resultado = false
            etMunicipio.error = MENSAJE_CAMPO_VACIO
        }
        if (etCodigoPostal.text?.isEmpty()!!) {
            resultado = false
            etCodigoPostal.error = MENSAJE_CAMPO_VACIO
        }
        if (etDireccion.text?.isEmpty()!!) {
            resultado = false
            etDireccion.error = MENSAJE_CAMPO_VACIO
        }
        if (etFecha.text?.isEmpty()!!) {
            resultado = false
            etFecha.error = MENSAJE_CAMPO_VACIO
        }
        if (etHora.text?.isEmpty()!!) {
            resultado = false
            etHora.error = MENSAJE_CAMPO_VACIO
        }

        return resultado
    }

    private fun subirAnexos() {
        //Deshabilita interfaz gráfica o, no cerrar el cuadro de diálogo
        btnEnviarReporte.isEnabled = false
        arrIdsAnexos.clear()
        // Una copia de los url, por si cancela
        arrUrlFotosBak.clear()
        arrUrlFotosBak.addAll(arrUrlFotos)
        subirImagen()  // -> subirVideo  -> subirAudio
    }

    // ENVIAR ANEXOS IMAGENES
    private fun subirImagen() {
        if (arrUrlFotos.isEmpty()) {
            subirVideo()
            return
        }

        //pbEsperaRE.visibility = View.VISIBLE
        habilitarEntrada(false)
        val uriImagen = arrUrlFotos.removeAt(0)
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val imgPath = getRealPathImageFromURI(uriImagen)
        val imgFile = File(imgPath)
        val tipoImagen = imgPath?.substring(imgPath?.lastIndexOf(".")+1)!!

        val direccion = URL_CREA_ATTACHMENT

        AndroidNetworking.upload(direccion)
            .addHeaders("Content-type","image/$tipoImagen")
            .addHeaders("Authorization", "Bearer $token")
            .addMultipartFile("file", imgFile)
            .addMultipartParameter("filename", "imagen.$tipoImagen")
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    //println("RESPONSE ATTACH: $response")
                    val idAnexo = response?.getInt("id")
                    if (idAnexo != null) {
                        arrIdsAnexos.add(idAnexo)
                    }
                    //pbEsperaRE.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    subirImagen()
                    actualizarListaAnexos()
                }

                override fun onError(anError: ANError?) {
                    cancelarEnviaReporte()
                    habilitarEntrada(true)
                    when (anError?.errorCode) {
                        ERROR_EXT_INVALIDA -> {
                            val jsonError = anError.errorBody as JSONObject
                            val mensaje = jsonError.getString("message")
                            Comun.mostrarMensaje(
                                this@ReporteEventoActiv, mensaje)
                        }
                        ERROR_TAM_INCORRECTO -> {
                            val jsonError = anError.errorBody as JSONObject
                            val arrError = jsonError.getJSONArray("file")
                            val mensaje = arrError[0] as String
                            Comun.mostrarMensaje(
                                this@ReporteEventoActiv, mensaje)
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@ReporteEventoActiv, MENSAJE_NO_INTERNET)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@ReporteEventoActiv, MENSAJE_ERROR_GENERAL)
                        }
                    }
                }

            })
    }

    private fun cancelarEnviaReporte() {
        arrIdsAnexos.clear()  // Intenta nuevamente
        arrUrlFotos.clear()
        arrUrlFotos.addAll(arrUrlFotosBak)
        actualizarListaAnexos()
        btnEnviarReporte.isEnabled = true
        btnSeleccionarAnexos.isEnabled = true
    }

    private fun subirVideo() {
        if (urlVideo == null) {
            subirAudio()
            return
        }

        //pbEsperaRE.visibility = View.VISIBLE
        habilitarEntrada(false)

        val uriVideo = urlVideo!!
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val videoPath = getRealPathVideoFromURI(uriVideo)
        val videoFile = File(videoPath)
        val tipoVideo = videoPath?.substring(videoPath?.lastIndexOf(".")+1)!!

        val direccion = URL_CREA_ATTACHMENT

        AndroidNetworking.upload(direccion)
            .addHeaders("Content-type","application/x-www-form-urlencoded")
            .addHeaders("Authorization", "Bearer $token")
            .addMultipartFile("file", videoFile)
            .addMultipartParameter("filename", "video.$tipoVideo")
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    habilitarEntrada(true)
                    val idAnexo = response?.getInt("id")
                    if (idAnexo != null) {
                        arrIdsAnexos.add(idAnexo)
                    }
                    urlVideo = null // ya lo subió
                    actualizarListaAnexos()
                    //pbEsperaRE.visibility = View.INVISIBLE

                    subirAudio()
                }

                override fun onError(anError: ANError?) {
                    cancelarEnviaReporte()
                    habilitarEntrada(true)
                    when (anError?.errorCode) {
                        ERROR_EXT_INVALIDA -> {
                            val jsonError = anError.errorBody as JSONObject
                            val mensaje = jsonError.getString("message")
                            Comun.mostrarMensaje(
                                this@ReporteEventoActiv, mensaje)
                            subirAudio()
                        }
                        ERROR_TAM_INCORRECTO -> {
                            val jsonError = anError.errorBody as JSONObject
                            val arrError = jsonError.getJSONArray("file")
                            val mensaje = arrError[0] as String
                            Comun.mostrarMensaje(this@ReporteEventoActiv, mensaje)
                        }
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@ReporteEventoActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(this@ReporteEventoActiv, MENSAJE_NO_INTERNET)
                        }
                        else -> {
                            Comun.mostrarMensaje(this@ReporteEventoActiv, MENSAJE_ERROR_GENERAL)
                        }
                    }
                }
            })
    }

    private fun subirAudio() {
        if (urlAudio == null) {
            pbEsperaRE.visibility = View.INVISIBLE
            subirDatosReporte()
            return
        }

        //pbEsperaRE.visibility = View.VISIBLE
        habilitarEntrada(false)

        val uriAudio = urlAudio!!
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val audioPath = getRealPathVideoFromURI(uriAudio)
        val audioFile = File(audioPath)
        val tipoAudio = audioPath?.substring(audioPath?.lastIndexOf(".")+1)!!

        val direccion = URL_CREA_ATTACHMENT

        AndroidNetworking.upload(direccion)
            .addHeaders("Content-type","application/x-www-form-urlencoded")
            .addHeaders("Authorization", "Bearer $token")
            .addMultipartFile("file", audioFile)
            .addMultipartParameter("filename", "audio.$tipoAudio")
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    val idAnexo = response?.getInt("id")
                    if (idAnexo != null) {
                        arrIdsAnexos.add(idAnexo)
                    }
                    //pbEsperaRE.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    urlAudio = null // Ya lo subió
                    actualizarListaAnexos()
                    subirDatosReporte()
                }

                override fun onError(anError: ANError?) {
                    cancelarEnviaReporte()
                    habilitarEntrada(true)
                    when (anError?.errorCode) {
                        ERROR_EXT_INVALIDA -> {
                            val jsonError = anError.errorBody as JSONObject
                            val mensaje = jsonError.getString("message")
                            Comun.mostrarMensaje(
                                this@ReporteEventoActiv, mensaje)
                        }
                        ERROR_TAM_INCORRECTO -> {
                            val jsonError = anError.errorBody as JSONObject
                            val arrError = jsonError.getJSONArray("file")
                            val mensaje = arrError[0] as String
                            Comun.mostrarMensaje(
                                this@ReporteEventoActiv, mensaje)
                        }
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@ReporteEventoActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@ReporteEventoActiv, MENSAJE_NO_INTERNET)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@ReporteEventoActiv, MENSAJE_ERROR_GENERAL)
                        }
                    }
                }
            })
    }


    private fun subirDatosReporte() {
        if (tipoEvento==TipoEvento.CAPTURA) {
            subirDatosReporteNuevo()
        } else if (tipoEvento==TipoEvento.COMPLEMENTO) {
            subirDatosReporteComplemento()
        }
    }

    private fun subirDatosReporteComplemento() {
        //pbEsperaRE.visibility = View.VISIBLE
        habilitarEntrada(false)
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        // Ya se validaron los campos
        val jsonBody = JSONObject("""{
            'incident': $idEvento,
            'title' : '${etNombreCorto.text}',
            'description' : '${etDescripcion.text.toString()}',
            'date' : '${etFecha.text.toString()}',
            'time' : '${etHora.text.toString()}',
            'attachments' : $arrIdsAnexos
            }
        """.trimIndent())

        val direccion = URL_CREA_EVENTO_COMPLEMENTO
            .replace(":id","$idEvento")

        AndroidNetworking.post(direccion)
            .addHeaders("Content-type","application/json")
            .addHeaders("Authorization", "Bearer $token")
            .addJSONObjectBody(jsonBody)
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    btnEnviarReporte.isEnabled = true
                    btnSeleccionarAnexos.isEnabled = true
                    //pbEsperaRE.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    Comun.mostrarMensaje(this@ReporteEventoActiv,
                        "El complemento se ha subido con éxito", true)
                }

                override fun onError(anError: ANError?) {
                    //pbEsperaRE.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    btnEnviarReporte.isEnabled = true
                    btnSeleccionarAnexos.isEnabled = true
                    when (anError?.errorCode) {
                        ERROR_CREA_EVENTO -> {
                            val jsonError = anError.errorBody as JSONObject
                            val mensaje = jsonError.getString("message")
                            Comun.mostrarMensaje(
                                this@ReporteEventoActiv, mensaje)
                        }
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@ReporteEventoActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@ReporteEventoActiv, MENSAJE_NO_INTERNET)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@ReporteEventoActiv, MENSAJE_ERROR_GENERAL)
                        }
                    }
                }
            })
    }

    // Subir información completa del reporte
    private fun subirDatosReporteNuevo() {
        //pbEsperaRE.visibility = View.VISIBLE
        habilitarEntrada(false)

        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")
        val idIsla = prefs.getInt(LLAVE_ID_ISLA_SELECCIONADA, -1)

        val descripcion = etDescripcion.text.toString()
        //val strDescripcion = URLEncoder.encode(descripcion, "UTF-8")
        // Ya se validaron los campos
        val jsonBody = JSONObject("""{
            'title' : '${etNombreCorto.text}',
            'category': '${spTipoEvento.selectedItem}',
            'description' : '$descripcion',
            'date' : '${etFecha.text.toString()}',
            'time' : '${etHora.text.toString()}',
            'location_latitude' : ${posicon_actual?.latitude},
            'location_longitude' : ${posicon_actual?.longitude},
            'location_address' : '${etDireccion.text.toString()}',
            'location_state' : '${etEstado.text.toString()}',
            'location_city' : '${etMunicipio.text.toString()}',
            'location_country' : '${etPais.text.toString()}',
            'location_zip_code' : '${etCodigoPostal.text.toString()}',
            'attachments' : $arrIdsAnexos,
            'group_id' : $idIsla
            }
        """.trimIndent())

        val direccion = URL_CREA_EVENTO

        AndroidNetworking.post(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .addJSONObjectBody(jsonBody)
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    btnEnviarReporte.isEnabled = true
                    btnSeleccionarAnexos.isEnabled = true
                    //pbEsperaRE.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    Comun.mostrarMensaje(this@ReporteEventoActiv,
                        "El evento se ha subido con éxito", true)
                }

                override fun onError(anError: ANError?) {
                    //pbEsperaRE.visibility = View.INVISIBLE
                    habilitarEntrada(true)

                    btnEnviarReporte.isEnabled = true
                    btnSeleccionarAnexos.isEnabled = true
                    when (anError?.errorCode) {
                        ERROR_CREA_EVENTO -> {
                            val jsonError = anError.errorBody as JSONObject
                            val mensaje = jsonError.getString("message")
                            Comun.mostrarMensaje(
                                this@ReporteEventoActiv, mensaje)
                        }
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@ReporteEventoActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@ReporteEventoActiv, MENSAJE_NO_INTERNET)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@ReporteEventoActiv, MENSAJE_ERROR_GENERAL)
                        }
                    }
                }
            })
    }

    companion object {
        open class DescargaDireccion(val contexto: Context) :
            AsyncTask<Location, Void, Address?>() {

            override fun doInBackground(vararg params: Location): Address? {

                val posicion = params[0]
                val geocoder = Geocoder(contexto, Locale.getDefault())
                var direcciones: List<Address> = emptyList()

                try {
                    direcciones = geocoder.getFromLocation(posicion.latitude, posicion.longitude, 2)
                } catch (ioException: IOException) {
                    return null
                } catch (illegalArgumentException: IllegalArgumentException) {
                    return null
                }
                // Procesa la dirección
                if (direcciones.isNotEmpty()) {
                    //println(direcciones[0])
                    return direcciones[0]
                }
                return null
            }
        }
    }

    private fun getRealPathImageFromURI(contentURI: Uri): String? {
        val result: String?
        val cursor: Cursor? = contentResolver.query(contentURI, null, null, null, null)
        if (cursor == null) {
            result = contentURI.path
        } else {
            cursor.moveToFirst()
            val idx: Int = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            result = cursor.getString(idx)
            cursor.close()
        }
        return result
    }


    private fun getRealPathVideoFromURI(uri: Uri): String? {
        uri.let { returnUri ->
            contentResolver.query(returnUri, null, null, null, null)
        }?.use { cursor ->
            val path = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            cursor.moveToFirst()
            return cursor.getString(path)
        }
        return null
    }

    private var entradaHabilitada = true

    private fun habilitarEntrada(habilitada: Boolean) {
        pbEsperaRE.visibility = if (habilitada) View.INVISIBLE else View.VISIBLE
        entradaHabilitada = habilitada
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        return if (entradaHabilitada) super.dispatchTouchEvent(ev) else true
    }
}

