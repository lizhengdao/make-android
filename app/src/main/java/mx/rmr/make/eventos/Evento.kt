package mx.rmr.make.eventos

class Evento (val titulo: String, val fecha: String, val idEvento: Int,
              val tipo: TipoRegistro, val alias: String, val idPrincipal: Int? = null)
{

}

enum class TipoRegistro
{
    PRINCIPAL,
    COMPLEMENTO,
}
