package mx.rmr.make.eventos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.activity_visualiza_anexo.*
import mx.rmr.make.DIRECCION_ANEXO
import mx.rmr.make.R

class VisualizaAnexo : AppCompatActivity()
{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_visualiza_anexo)

        val direccion = intent.getStringExtra(DIRECCION_ANEXO)
        mostrarAnexo(direccion)
    }

    private fun mostrarAnexo(direccion: String?) {
        webAnexo.settings.loadWithOverviewMode = true;
        webAnexo.settings.useWideViewPort = true;
        webAnexo.settings.builtInZoomControls = true
        webAnexo.setInitialScale(1)
        webAnexo.loadUrl(direccion)

        webAnexo.webViewClient = object: WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                pbVisualizaAnexo.visibility = View.INVISIBLE
            }
        }
    }
}
