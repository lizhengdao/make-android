package mx.rmr.make.eventos

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONArrayRequestListener
import com.androidnetworking.interfaces.OkHttpResponseListener
import kotlinx.android.synthetic.main.activity_eventos.*
import mx.rmr.make.*
import mx.rmr.make.util.Comun
import okhttp3.Response
import org.json.JSONArray
import org.json.JSONObject

// Lista de eventos para esta isla
class EventosActiv : AppCompatActivity(), ListenerConsultaEvento
{
    private var adaptadorEventos: AdaptadorEventos? = null

    private var arrEventos = mutableListOf<Evento>()
    private var arrEventosJson = mutableListOf<JSONObject>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_eventos)

        // Recycler
        var layout = LinearLayoutManager(this)
        layout.orientation = LinearLayoutManager.VERTICAL
        rvEventos.layoutManager = layout

        adaptadorEventos = AdaptadorEventos(this, arrayOf())
        rvEventos.adapter = adaptadorEventos

        val divisor = DividerItemDecoration(this, layout.orientation)
        rvEventos.addItemDecoration(divisor)
    }

    // Cada que entra a esta ventana, recarga los datos
    override fun onResume() {
        super.onResume()
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE) ?: return
        val idIsla = prefs.getInt(LLAVE_ID_ISLA_SELECCIONADA, -1)
        descargarEventos(idIsla)
    }

    private fun descargarEventos(idIsla: Int) {
        //pbEspera.visibility = View.VISIBLE
        habilitarEntrada(false)

        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val direccion = URL_GRUPO_INCIDENTES.replace(":id", "$idIsla")

        AndroidNetworking.get(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .build()
            .getAsJSONArray(object: JSONArrayRequestListener {
                override fun onResponse(response: JSONArray?) {
                    //pbEspera.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    crearArregloIncidentes(response)
                }

                override fun onError(anError: ANError?) {
                    //pbEspera.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    when (anError?.errorCode) {
                        ERROR_NO_HAY_EVENTOS -> {
                            Comun.mostrarMensaje(
                                this@EventosActiv, MENSAJE_NO_ISLAS)
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@EventosActiv, MENSAJE_NO_INTERNET)
                        }
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@EventosActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@EventosActiv, MENSAJE_ERROR_GENERAL)
                        }
                    }
                }
            })
    }

    private fun crearArregloIncidentes(response: JSONArray?) {

        if (response?.length()==0) {
            Comun.mostrarMensaje(this, MENSAJE_NO_ISLAS)
            tvEventosIsla.text = "Esta isla no tiene eventos"
            return
        }

        arrEventos.clear()
        arrEventosJson.clear()

        val size = response?.length() ?: return

        for (i in 0 until size) {
            val dEvento = response?.getJSONObject(i)
            arrEventosJson.add(dEvento)
            val titulo = dEvento.getString("title")
            val fecha = dEvento.getString("date")
            val idEvento = dEvento.getInt("id")
            val alias = dEvento.getString("user")
            val evento = Evento(titulo, fecha, idEvento, TipoRegistro.PRINCIPAL, alias)
            arrEventos.add(evento)

            if (i==0) {
                val nombreIsla = dEvento.getString("group")
                tvEventosIsla.text = "Eventos de la isla: $nombreIsla"
            }

            // Complementos
            val complementos = dEvento.getJSONArray("complements")
            val idPrincipal = idEvento
            var numeroComplementos = complementos?.length() ?: 0
            for (k in 0 until numeroComplementos) {
                val dComplemento = complementos.getJSONObject(k)
                val titulo = dComplemento.getString("title")
                val fecha = dComplemento.getString("date")
                val idEvento = dComplemento.getInt("id")
                val evento = Evento(titulo, fecha, idEvento, TipoRegistro.COMPLEMENTO, alias, idPrincipal)
                arrEventos.add(evento)
            }
        }

        adaptadorEventos?.arrEventos = arrEventos.toTypedArray()
        adaptadorEventos?.notifyDataSetChanged()
    }

    // Transición a otras actividades (Captura, Consulta, Complemento)
    fun mostrarReporteEventoCaptura(v: View) {
        val intReporteEvento = Intent(this, ReporteEventoActiv::class.java)
        intReporteEvento.putExtra(LLAVE_TIPO_REPORTE_EVENTO, TipoEvento.CAPTURA)
        startActivity(intReporteEvento)
    }

    // Complemento de un evento
    fun mostrarReporteEventoComplemento(v: View) {
        val adaptador = (adaptadorEventos as AdaptadorEventos)
        if (adaptador.arrEventos.isNotEmpty()) {
            val indice = (adaptadorEventos as AdaptadorEventos).indiceSeleccionado
            if (indice != null) {
                val intReporteEvento = Intent(this, ReporteEventoActiv::class.java)
                intReporteEvento.putExtra(LLAVE_TIPO_REPORTE_EVENTO, TipoEvento.COMPLEMENTO)
                // Complemento para este evento
                val dEvento = buscarEvento(indice)
                intReporteEvento.putExtra(EVENTO_CONSULTA_STR, dEvento.toString())
                startActivity(intReporteEvento)
            } else {
                Comun.mostrarMensaje(this, "Selecciona un evento para crear un complemento")
            }
        } else {
            Comun.mostrarMensaje(this, "No hay eventos para crear complementos")
        }
    }

    override fun onPause() {
        pbEspera.visibility = View.INVISIBLE
        super.onPause()
    }

    override fun btnClicked(indice: Int) {
        val dEvento = buscarEvento(indice)
        val intReporteEvento = Intent(this, ReporteEventoActiv::class.java)
        intReporteEvento.putExtra(EVENTO_CONSULTA_STR, dEvento.toString())
        intReporteEvento.putExtra(LLAVE_TIPO_REPORTE_EVENTO, TipoEvento.CONSULTA)
        startActivity(intReporteEvento)
    }

    override fun btnBorrarClicked(posicion: Int, idEvento: Int) {
        val dialogo = AlertDialog.Builder(this)
            .setMessage("¿Seguro que quiere borrar este evento?")
            .setNegativeButton("Si", DialogInterface.OnClickListener() {
                    _: DialogInterface, _: Int ->
                subirBorrarEvento(idEvento)   // -> borrarEventoArreglo
            })
            .setPositiveButton("No", null)
            .setCancelable(false)
            .create()
        dialogo.show()
    }

    private fun subirBorrarEvento(idEvento: Int) {
        //pbEspera.visibility = View.VISIBLE
        habilitarEntrada(false)

        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val direccion = URL_INCIDENTE_BORRAR + "$idEvento"

        AndroidNetworking.delete(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .build()
            .getAsOkHttpResponse(object: OkHttpResponseListener {
                override fun onResponse(response: Response?) {
                    //pbEspera.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    Comun.mostrarMensaje(this@EventosActiv, "El evento se ha borrado con éxito")
                    borrarEventoArreglo(idEvento)
                }

                override fun onError(anError: ANError?) {
                    //pbEspera.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    when (anError?.errorCode) {
                        ERROR_REGISTRO_BORRAR_NO_EXISTE -> {
                            Comun.mostrarMensaje(this@EventosActiv, MENSAJE_REGISTRO_NO_EXISTE)
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(this@EventosActiv, MENSAJE_NO_INTERNET)
                        }
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(this@EventosActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@EventosActiv, MENSAJE_ERROR_GENERAL)
                        }
                    }
                }
            })
    }

    private fun borrarEventoArreglo(idEvento: Int): Unit {
        // Quitar el JSON con este id y crear nuevamente del adaptador
        for (i in 0 until arrEventosJson.size) {
            val jsonEvento = arrEventosJson[i]
            val idJson = jsonEvento.getInt("id")
            if (idJson == idEvento) {
                arrEventosJson.removeAt(i)
                break
            }
        }
        // Reconstruir adaptador
        if (arrEventosJson.size == 0) {
            tvEventosIsla.text = "Esta isla no tiene eventos"
        }

        arrEventos.clear()
        var esInicio = true
        for (dEvento in arrEventosJson) {
            val titulo = dEvento.getString("title")
            val fecha = dEvento.getString("date")
            val idEvento = dEvento.getInt("id")
            val alias = dEvento.getString("user")
            val evento = Evento(titulo, fecha, idEvento, TipoRegistro.PRINCIPAL, alias)
            arrEventos.add(evento)

            if (esInicio) {
                val nombreIsla = dEvento.getString("group")
                tvEventosIsla.text = "Eventos de la isla: $nombreIsla"
                esInicio = false
            }

            // Complementos
            val complementos = dEvento.getJSONArray("complements")
            val idPrincipal = idEvento
            var numeroComplementos = complementos?.length() ?: 0
            for (k in 0 until numeroComplementos) {
                val dComplemento = complementos.getJSONObject(k)
                val titulo = dComplemento.getString("title")
                val fecha = dComplemento.getString("date")
                val idEvento = dComplemento.getInt("id")
                val evento =
                    Evento(titulo, fecha, idEvento, TipoRegistro.COMPLEMENTO, alias, idPrincipal)
                arrEventos.add(evento)
            }
        }

        adaptadorEventos?.arrEventos = arrEventos.toTypedArray()
        adaptadorEventos?.notifyDataSetChanged()
    }

    private fun buscarEvento(indice: Int): JSONObject? {
        val evento = arrEventos[indice]
        val idEvento = evento.idEvento
        for (dEvento in arrEventosJson) {
            val id = dEvento.getInt("id")   // Del arreglo
            if (evento.tipo==TipoRegistro.PRINCIPAL) {
                if (id == idEvento) {
                    return dEvento
                }
            } else {    // Complemento
                val idPrincipal = evento.idPrincipal    // Principal
                if (id==idPrincipal) {
                    // Buscar en complemento
                    return crearEventoComplemento(dEvento, idEvento );
                }
            }
        }
        return null
    }

    private fun crearEventoComplemento(
        dEvento: JSONObject, idBuscado: Int
    ): JSONObject? {
        val arrComplementos = dEvento.getJSONArray("complements")

        var nuevoEvento = JSONObject(dEvento.toString())    // Copia del principal
        // Agregarle los datos del complemento
        for (i in 0 until arrComplementos.length()) {
            var eventoComplemento = arrComplementos[i] as JSONObject
            val idComplemento = eventoComplemento.getInt("id")
            if (idComplemento == idBuscado) {
                nuevoEvento.put("title", eventoComplemento.getString("title"))
                nuevoEvento.put("description", eventoComplemento.getString("description"))
                nuevoEvento.put("date", eventoComplemento.getString("date"))
                nuevoEvento.put("time", eventoComplemento.getString("time"))
                nuevoEvento.put("attachments", eventoComplemento.getJSONArray("attachments"))
                return nuevoEvento
            }
        }
        return null
    }

    private var entradaHabilitada = true

    private fun habilitarEntrada(habilitada: Boolean) {
        pbEspera.visibility = if (habilitada) View.INVISIBLE else View.VISIBLE
        entradaHabilitada = habilitada
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        return if (entradaHabilitada) super.dispatchTouchEvent(ev) else true
    }
}
