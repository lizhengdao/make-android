package mx.rmr.make.plan

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.FileProvider
import kotlinx.android.synthetic.main.activity_plan_grafico.*
import mx.rmr.make.util.Comun
import mx.rmr.make.LLAVE_JSON_ISLA
import mx.rmr.make.R
import org.json.JSONArray
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin

class PlanGraficoActiv : AppCompatActivity()
{
    private lateinit var plan: JSONObject
    private lateinit var bitmapPlan: Bitmap

    private var dRolesUsados = mutableMapOf<Int, String>()
    private var dRolColor = mutableMapOf<Int, Int>()
    private var siguienteLlaveColor = 1

    private var personaAusente = ""

    private var mapaModelo = mapOf("democratic" to "Democrático",
        "centralized" to "Centralizado", "weighted" to "Ponderado")

    // Colores para cada rol
    private val colores = mapOf<Int, Int>(0 to Color.rgb(128,128, 128),
        1 to Color.rgb(204, 204, 0),
        2 to Color.rgb(255, 72, 26),
        3 to Color.rgb(26, 209, 255),
        4 to Color.rgb(255, 255, 76),
        5 to Color.rgb(153, 205, 255),
        6 to Color.rgb(255, 76, 255),
        7 to Color.rgb(76, 255, 76),
        8 to Color.rgb(171, 255, 123),
        9 to Color.rgb(207, 123, 255),
        10 to Color.rgb(255, 123, 171),
        11 to Color.rgb(123, 255, 207),
        12 to Color.rgb(255, 150, 50)
    )

    val ALTO_FUENTE = 15f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plan_grafico)

        // Extraer JSON
        val strJSON = intent.getStringExtra(LLAVE_JSON_ISLA) ?: "{}"
        plan = JSONObject(strJSON)
    }

    override fun onStart() {
        super.onStart()
        try {
            dibujarPlan()
        } catch (e: Exception) {
            Comun.mostrarMensaje(this, "No hay información para mostrar este plan de seguridad", true)
        }
    }

    private fun dibujarPlan() {
        val detalles = plan.getJSONObject("governance_details")
        val modeloGobernanza = detalles.getString("winner")

        val aliasCentralizado = "desconocido"
        if (plan.has("group_centralized_person")) {
            plan.getString("group_centralized_person")
        }
        val dPesos = plan.getJSONObject("group_user_weights")

        this.dRolesUsados.clear()
        this.dRolColor.clear()
        this.siguienteLlaveColor = 1;   // El cero es gris

        val floralWhite = Color.rgb(255, 250, 240)
        val config = Bitmap.Config.ARGB_8888
        bitmapPlan = Bitmap.createBitmap(650, 950, config)
        val canvas = Canvas(bitmapPlan)
        val p = Paint()
        p.isAntiAlias = true
        p.color = floralWhite
        canvas.drawRect(2f, 2f, 646f, 946f, p)
        p.color = Color.GRAY
        p.style = Paint.Style.STROKE
        p.strokeWidth = 3f
        canvas.drawRect(0f, 0f, 649f, 719f, p)

        val personaAusente = plan.getString("missing_person")
        this.personaAusente = personaAusente

        // Circulo central
        p.color = Color.BLACK
        val rectImagen = Rect(0, 0, 650, 750)
        val radio = 70f
        val centro = Point(rectImagen.exactCenterX().toInt(), rectImagen.exactCenterY().toInt())
        canvas.drawCircle(rectImagen.exactCenterX(), rectImagen.exactCenterY(), radio, p)

        // Texto
        dibujarTexto(canvas, centro, personaAusente, Color.BLACK, 2 * radio, true)

        // n miembros alrededor
        val rolesMiembro = plan.getJSONObject("members_roles")
        val n = rolesMiembro.length()
        if (n > 0) {
            val pasoAngulo = 360 / n
            var i = 0
            for (roles in rolesMiembro.keys()) {
                val arrRoles = rolesMiembro.getJSONArray(roles)
                val aliasRol = roles
                agregarRolesUsados(arrRoles)
                val alfa = i * pasoAngulo * PI / 180
                var peso = 1
                if (modeloGobernanza == "weighted") {
                    peso = dPesos.getInt(aliasRol)
                } else if (modeloGobernanza == "centralized" && aliasRol == aliasCentralizado) {
                    peso = 4
                }
                dibujarCirculosRoles(canvas, rectImagen, alfa, aliasRol, arrRoles, peso)
                i++
            }
        }
        agregarTitulosPlanGrafico(canvas, rectImagen, modeloGobernanza)

        imgPlan.setImageBitmap(bitmapPlan)
    }

    private fun agregarTitulosPlanGrafico(canvas: Canvas, rect: Rect, modelo: String) {
        // Títulos del documento
        dibujarTexto(
            canvas,
            Point(rect.centerX(), (ALTO_FUENTE*1.5f).toInt()),
            "Plan de seguridad para '$personaAusente'",
            Color.BLACK,
            rect.right.toFloat(),
            true
        )

        dibujarTexto(
            canvas,
            Point(rect.centerX(),(ALTO_FUENTE*3.5).toInt()),
            "Modelo de gobernanza: ${mapaModelo[modelo]}",
            Color.BLACK,
            rect.right.toFloat(),
            true
        )

        val INICIO_Y = 745f
        var px = 20f
        var py = INICIO_Y
        val radio = 15f
        var primeraColumna = true
        var numeroLinea = 0
        val rolesUsados = dRolesUsados.toMutableMap()
        val maximoRoles = minOf(rolesUsados.size, 12)
        var rolesAgregados = 0
        val p = Paint()
        p.isAntiAlias = true
        for (k: Int in rolesUsados.keys) {
            // Círculo
            val indiceColor = dRolColor.get(k) ?: 0
            val rolColor = colores[indiceColor] ?: 0
            p.color = rolColor
            p.style = Paint.Style.FILL
            canvas.drawCircle(px,py,radio,p)

            // Extraer el nombre de rol
            val nombreRol = dRolesUsados[k] ?: "NA"

            // Rol
            dibujarTexto(canvas, Point(px.toInt()+(1.2*radio).toInt(), py.toInt()+(radio/2).toInt()),
                nombreRol, Color.BLACK, (canvas.width/2-radio*2.5f).toFloat(), false)

            py += radio*2.5f

            if (numeroLinea >= (maximoRoles+1)/2 - 1 && primeraColumna) {
                px += rect.centerX()
                py = INICIO_Y
                primeraColumna = false
            }

            numeroLinea += 1

            rolesAgregados += 1
            if (rolesAgregados >= maximoRoles) {
                break
            }
        }
    }

    private fun dibujarCirculosRoles(canvas: Canvas, rect: Rect, alfa: Double, aliasRol: String?, arrRoles: JSONArray, peso: Int) {
        val radioAlias = 200
        var indiceColor: Int
        var numeroRoles = arrRoles.length()
        val RADIO_CENTRO = 70
        val RADIO_INICIO = RADIO_CENTRO + 10*peso
        var listaRadios = mutableListOf<Int>(RADIO_INICIO)      // radio para cada alias

        for (i in 1 until numeroRoles) {
            listaRadios.add(RADIO_INICIO - 10*i)
        }

        var trazarLinea = true
        var i = 0

        for (indice in 0 until arrRoles.length()) {
            val rol = arrRoles.getJSONObject(indice)
            val radio = listaRadios[i]
            indiceColor = rol.getInt("role_id")
            val id_rol = rol.getInt("role_id")
            val id_color = dRolColor.get(id_rol)
            if (id_color==null) {
                if (siguienteLlaveColor < colores.size) {   // hay color?
                    this.dRolColor[id_rol] = siguienteLlaveColor
                    siguienteLlaveColor += 1
                } else {
                    this.dRolColor[id_rol] = 0  // gris
                }
            }

            if (!colores.containsKey(indiceColor)) {
                indiceColor = 0
            }
            val colorRol = colores.get(dRolColor.get(id_rol))

            // Círculo
            val p = Paint()
            p.isAntiAlias = true
            p.style = Paint.Style.FILL
            p.color = colorRol!!
            indiceColor++

            val px = radioAlias* cos(alfa) + rect.right/2
            val py = rect.bottom/2 - radioAlias* sin(alfa)

            canvas.drawCircle(px.toFloat(), py.toFloat(), radio.toFloat(), p)

            // Circunferencia del rol
            p.color = Color.GRAY
            p.style = Paint.Style.STROKE
            p.strokeWidth = 1f
            canvas.drawCircle(px.toFloat(), py.toFloat(), radio.toFloat(), p)

            // Alias
            if (indice==arrRoles.length()-1) {
                dibujarTexto(
                    canvas,
                    Point(px.toInt(),py.toInt()),
                    aliasRol!!,
                    Color.BLACK,
                    (2*radio).toFloat(),
                    true
                )
            }

            // Líneas
            if (trazarLinea) {
                trazarLinea = false
                // Línea
                val centro = Point(rect.right/2, rect.bottom/2)
                p.color = Color.BLACK
                p.strokeWidth = 3f
                val centroCirculo = Point((centro.x + 70* cos(alfa)).toInt(), (centro.y - 70* sin(alfa)).toInt())
                val centroAlias = Point((px - radio* cos(alfa)).toInt(), (py + radio* sin(alfa)).toInt())

                canvas.drawLine(centroCirculo.x.toFloat(), centroCirculo.y.toFloat(),centroAlias.x.toFloat(), centroAlias.y.toFloat(), p)
            }

            i += 1
        }
    }

    private fun agregarRolesUsados(arrRoles: JSONArray) {

        for (i in 0 until arrRoles.length()) {
            val dRol = arrRoles.getJSONObject(i)
            val idRol = dRol.getInt("role_id")
            val nombreRol = dRol.getString("role")
            this.dRolesUsados[idRol] = nombreRol
        }
    }

    private fun dibujarTexto(
        canvas: Canvas,
        centro: Point,
        cadena: String,
        color: Int,
        ancho: Float,
        centrado: Boolean
    ) {
        val p = Paint()
        p.color = color
        p.textSize = 24f
        p.isAntiAlias = true
        p.typeface = Typeface.DEFAULT

        p.textAlign = if (centrado) Paint.Align.CENTER else Paint.Align.LEFT

        if (p.measureText(cadena)>ancho) {
            do {
                p.textSize -= 1
            } while (p.measureText(cadena) >= ancho)
        }

        canvas.drawText(cadena, centro.x.toFloat(), centro.y.toFloat(), p)
    }

    fun compartirPlanGrafico(v: View) {
        val bos = ByteArrayOutputStream();
        bitmapPlan.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        val jpgFile = File.createTempFile("plan_", ".jpg")

        try {
            val fos = FileOutputStream(jpgFile);
            fos.write(bos.toByteArray());
        } catch (e: IOException) {
            e.printStackTrace();
        }

        var jpgUri = FileProvider.getUriForFile(this, "$packageName.provider", jpgFile)
        grantUriPermission(packageName, jpgUri, Intent.FLAG_GRANT_READ_URI_PERMISSION)

        val intCompartir = Intent()
        intCompartir.action = Intent.ACTION_SEND
        intCompartir.type = "image/jpeg"
        intCompartir.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        intCompartir.putExtra(Intent.EXTRA_STREAM, jpgUri)
        intCompartir.putExtra(Intent.EXTRA_SUBJECT, "Plan de seguridad")
        intCompartir.putExtra(Intent.EXTRA_TEXT, "Plan de seguridad para '$personaAusente'")
        val chooser = Intent.createChooser(intCompartir, "Compartir plan de seguridad")

        val resInfoList = this.packageManager
            .queryIntentActivities(chooser, PackageManager.MATCH_DEFAULT_ONLY)
        for (resolveInfo in resInfoList) {
            val packageName = resolveInfo.activityInfo.packageName
            grantUriPermission(
                packageName,
                jpgUri,
                Intent.FLAG_GRANT_READ_URI_PERMISSION
            )
        }

        startActivity(chooser)
    }
}
