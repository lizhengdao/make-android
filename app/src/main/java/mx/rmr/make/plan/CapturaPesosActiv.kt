package mx.rmr.make.plan

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_captura_pesos.*
import kotlinx.android.synthetic.main.activity_eventos.*
import mx.rmr.make.LLAVE_ARR_ALIAS
import mx.rmr.make.LLAVE_ARR_PESOS
import mx.rmr.make.R
import mx.rmr.make.eventos.AdaptadorEventos

class CapturaPesosActiv : AppCompatActivity()
{
    lateinit var adaptadorPesos: AdaptadorPesos

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_captura_pesos)

        // extraer arrAlias
        val arrAlias = intent.getStringArrayExtra(LLAVE_ARR_ALIAS)

        var layout = LinearLayoutManager(this)
        layout.orientation = LinearLayoutManager.VERTICAL
        rvPesos.layoutManager = layout

        adaptadorPesos = AdaptadorPesos(this, arrAlias)
        rvPesos.adapter = adaptadorPesos

        val divisor = DividerItemDecoration(this, layout.orientation)
        rvPesos.addItemDecoration(divisor)
    }

    fun terminarCaptura(v: View) {
        val intRegreso = Intent()
        intRegreso.putExtra(LLAVE_ARR_PESOS, adaptadorPesos.arrPesos.toIntArray())
        setResult(Activity.RESULT_OK, intRegreso)
        finish()
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        val intRegreso = Intent()
        intRegreso.putExtra(LLAVE_ARR_PESOS, adaptadorPesos.arrPesos.toIntArray())
        setResult(Activity.RESULT_OK, intRegreso)
        finish()
    }
}
