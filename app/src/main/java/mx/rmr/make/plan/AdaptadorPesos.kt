package mx.rmr.make.plan

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.rv_renglon_peso.view.*
import mx.rmr.make.R

class AdaptadorPesos (private val contexto: Context, var arrAlias: Array<String>):
RecyclerView.Adapter<AdaptadorPesos.RenglonPeso>()
{
    var arrPesos: MutableList<Int> = MutableList(arrAlias.size) { 1 }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RenglonPeso {
        val vista = LayoutInflater.from(parent.context).
        inflate(R.layout.rv_renglon_peso, parent,false)
        return RenglonPeso(vista)
    }

    override fun getItemCount(): Int {
        return arrAlias.size
    }

    override fun onBindViewHolder(holder: RenglonPeso, position: Int) {
        holder.vistaRenglon.tvAliasCapturaPeso.text = arrAlias[position]
        holder.vistaRenglon.tvPesoAlias.text = "${arrPesos[position]}"

        holder.vistaRenglon.btnMas.setOnClickListener {
            if (arrPesos[position]<5) {
                arrPesos[position]++
                holder.vistaRenglon.tvPesoAlias.text = "${arrPesos[position]}"
            }
        }
        holder.vistaRenglon.btnMenos.setOnClickListener {
            var peso = arrPesos[position]
            if (peso>1) {
                peso--
                arrPesos[position] = peso
                holder.vistaRenglon.tvPesoAlias.text = "$peso"
            }
        }
    }

    class RenglonPeso (var vistaRenglon: View): RecyclerView.ViewHolder(vistaRenglon)
    {

    }
}